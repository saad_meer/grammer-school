<aside id="sidebar" class="aside aside-fixed">
      <div class="aside-header">
        <a href="../../index.html" class="aside-logo">grammer <span>school</span></a>
        <a href="" class="aside-menu-link">
          <i data-feather="menu"></i>
          <i data-feather="x"></i>
        </a>
      </div>
      <div class="aside-body">
        <div class="aside-loggedin">
          <div class="d-flex align-items-center justify-content-start">
            <a href="" class="avatar">
                @if(auth()->user()->user_image!='')
                <img src="{{url('storage/'.auth()->user()->user_image)}}"  class="rounded-circle" alt="">
                @else
                <img src="https://via.placeholder.com/500" class="rounded-circle" alt="">

                @endif
            </a>
            <div class="aside-alert-link">
              <a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
            </div>
          </div>

          <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
          <div class="aside-loggedin-user">
            <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
              <h6 class="tx-semibold mg-b-0"></h6>
              <i data-feather="chevron-down"></i>
            </a>
            <p class="tx-color-03 tx-12 mg-b-0">{{auth()->user()->first_name.' '.auth()->user()->last_name}}</p>
          </div>
          <div class="collapse" id="loggedinMenu">
            <ul class="nav nav-aside mg-b-0">
              <li class="nav-item"><a href="{{url('admin/users/editprofile')}}" class="nav-link"><i data-feather="edit"></i> <span>Edit Profile</span></a></li>
              <li class="nav-item"><a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
            </ul>
          </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
          <li class="nav-label">Dashboard</li>
          @if(auth()->user()->role == 'admin')
          <li class="nav-item "><a href="{{url('admin/dashboard')}}" class="nav-link"><i data-feather="shopping-bag"></i> <span>Dashboard</span></a></li>
          <li class="nav-item {{ Request::is('admin/users/display') ? 'active' : '' }}"><a href="{{url('admin/users/display')}}" class="nav-link"><i data-feather="users"></i> <span>Users</span></a></li>
          @endif
          <li class="nav-label mg-t-25">Admin Links</li>
          <li class="nav-item {{ Request::is('admin/students/display') ? 'active' : '' }}"><a href="{{url('admin/students/display')}}" class="nav-link"><i data-feather="users"></i> <span>Students</span></a></li>
          <li class="nav-item {{ Request::is('admin/challan/display') ? 'active' : '' }}"><a href="{{url('admin/challan/display')}}" class="nav-link"><i data-feather="file"></i> <span>Challan</span></a></li> 
          <li class="nav-item {{ Request::is('admin/challan/downloaddisplay') ? 'active' : '' }}"><a href="{{url('admin/challan/downloaddisplay')}}" class="nav-link"><i data-feather="download"></i> <span>Downloads</span></a></li>
          @if(auth()->user()->role == 'admin')
          <li class="nav-item with-sub">
            <a href="" class="nav-link  {{ Request::is('admin/reports/student_list') ? 'active' : '' }} "><i data-feather="pie-chart"></i> <span>Reports</span></a>
            <ul>
              <li><a href="{{url('admin/reports/student_list')}}">Student List</a></li>
              <li><a href="{{url('admin/reports/challanlist')}}">Challan List</a></li>
            </ul>
          </li>
          @endif
          @if(auth()->user()->role == 'admin')
          <li class="nav-item {{ Request::is('admin/useractivity') ? 'active' : '' }}"><a href="{{url('admin/useractivity')}}" class="nav-link"><i data-feather="file"></i> <span>User Activity</span></a></li>
          <li class="nav-item {{ Request::is('admin/settings/add') ? 'active' : '' }}"><a href="{{url('admin/settings/add')}}" class="nav-link"><i data-feather="settings"></i> <span>Settings</span></a></li>
          @endif

         </ul>
      </div>
    </aside>
