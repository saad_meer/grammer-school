<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="School">
    <meta name="twitter:description" content="The Grammer School ">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="School">
    <meta property="og:description" content="The Grammer School ">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="The Grammer School ">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}">

    <title>School | @yield('title')</title>

    <!-- vendor css -->
    <link href="{{url('lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{url('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    @yield('datatables')
    <!-- DashForge CSS -->
    <link rel="stylesheet" href="{{url('assets/css/dashforge.css')}}">
    -    <link rel="stylesheet" href="{{url('assets/css/dashforge.dashboard.css')}}">
-    <link rel="stylesheet" href="{{url('assets/css/dashforge.demo.css')}}">
-    <link rel="stylesheet" href="{{url('admin/customadmin.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    @yield('styles')
  </head>
<style>

.swal-title {

font-size:16px !important;

}
</style>
  <body>

    <div  id="ajax_loader">Loading&#8230;</div>
  @include('layouts.sidebar')
  <div class="content  pd-0">
   @yield('content')
  </div>
    </body>
  <script src="{{url('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{url('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{url('lib/feather-icons/feather.min.js')}}"></script>
    <script src="{{url('lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script src="{{url('lib/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{url('lib/jquery.flot/jquery.flot.stack.js')}}"></script>
    <script src="{{url('lib/jquery.flot/jquery.flot.resize.js')}}"></script>
    <script src="{{url('lib/chart.js/Chart.bundle.min.js')}}"></script>
    <script src="{{url('lib/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{url('lib/jqvmap/maps/jquery.vmap.usa.js')}}"></script>

    <script src="{{url('assets/js/dashforge.js')}}"></script>
    <script src="{{url('assets/js/dashforge.aside.js')}}"></script>
  {{-- / <script src="{{url('assets/js/dashboard-one.js')}}"></script> --}}
    <script src="{{url('lib/jqueryui/jquery-ui.min.js')}}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="{{url('assets/sweetalert.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                    $("#ajax_loader").addClass('loading');
                    $("#sidebar").addClass('d-none');
                },
                complete: function () {
                    $("#ajax_loader").removeClass('loading');
                    $("#sidebar").removeClass('d-none');
                }
        });
        $('#summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 120,
        toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link','picture']],
          ['view', ['fullscreen']]
        ]
      });
    </script>
    @yield('scripts')
  </body>
</html>
