
@extends('layouts.header')
@section('title','Edit Student')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Student</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/students/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Edit Users" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/students/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Registration Number<span class="text-danger">*</span></label>
                                    <input type="text"  class="form-control" value="{{$student->registration_number}}"  name="registration_number" placeholder="Registration Number">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Student Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{$student->student_name}}" name="student_name" placeholder="Student Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Father Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{$student->father_name}}" name="father_name" placeholder="Father Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Father Cnic<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{$student->father_cnic}}" name="father_cnic" placeholder="Father Cnic">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">DOB<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" @if(!empty($student->dob)) value="{{date('d-m-Y',strtotime($student->dob))}}" @endif  placeholder="Choose date" name="dob" id="std_dob" autocomplete="off">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Class</label>
                                        <select class="custom-select" name="class">
                                            <option value="">Please Select Class</option>
                                            @foreach ($student_class as $std_class)
                                                <option value="{{$std_class->student_class}}" @if($student->class==$std_class->student_class)  selected @endif>{{$std_class->student_class}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Gender</label>
                                        <select class="custom-select" name="gender">
                                            <option value="Male" @if($student->gender=='Male') selected @endif>Male</option>
                                            <option value="Female" @if($student->gender=='Female') selected @endif>Female</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Transport Fee</label>
                                    <input type="text" class="form-control" value="{{$student->transport_fee}}" name="transport_fee" placeholder="Transport Fee">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Fee Discount</label>
                                    <input type="number" min="0" max="100" value="{{$student->transport_fee}}"  class="form-control" name="transport_fee" placeholder="Transport Fee">
                                    <div class="">This will be consider in percentage!</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="student_status">
                                            <option value="Active" @if($student->student_status=='Active') selected @endif>Active</option>
                                            <option value="Inactive" @if($student->student_status=='Inactive') selected @endif>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">
$(function(){
        'use strict'
        $('#std_dob').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            maxDate: new Date
        });

    });
$('#addform').submit(function(event) {
    var url = '{{url("/")}}';

//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $student->id; ?>');

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addform')[0].reset();
            swalsuccess(data.message,url+'/admin/students/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



