@if(!isset($header_not) || $header_not == 0)
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sale Order</title>
</head>
 <style>
  h1,h3,h5,h4{
    color: #000000;
    font-family: "Times New Roman", Times, serif;
  }
  p,td,span{
    color: #333333 !important;
  }
.pdf-body{
  position: relative;

  width: 100%;
  margin-right: auto;
  margin-left: auto;
  padding: 20px;
  font-family: "Times New Roman", Times, serif;

}
.pdf-border-div{
    border:1px solid;
    border-style: dashed;
}

h1{
  margin: 0px;

  text-align: left;
}
h5{
  margin: 0px;
  display: inline;
  font-weight: bold;
  font-size: 14px;
}
h5 span{
  margin: 0px;
  font-size: 13x;
  font-weight: normal;}

td {
  border-bottom: 1px solid #dddddd;
  padding: 8px;

  font-size: 14px;
  text-align: center;
   }
   th {
  border-bottom: 1px solid #dddddd;
  padding: 8px;
  text-align: center;
  font-size: 13px;

   }
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    position: relative;
}

  p{
   margin:1px;
   font-size: 13px;
  font-weight: normal;
 }
.logo{
  width: 50%;


}
.top-header{
position: relative;
padding: 20px; }
.side-text{
  width: 100%;
  margin-left: auto;


}

.ship_address{
 width: 40%

}





</style>
<body>

<div class="pdf-body">
@endif

    <div class="pdf-border-div" style="page-break-after: always;">

<table style="border-top: none; position: relative;">
    <thead>
        <tr>
        <th>Student Name</th>
        <th>Father Name</th>
        <th>Class</th>
        <th>Fee Month</th>
        <th>Actual Fee</th>
        <th>Transport</th>
        <th>Discount</th>
        <th>Discount Fee</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody id="student_record">
        <tr>
            <th>{{$data->student_name}}</th>
            <th>{{$data->father_name}}</th>
            <th>{{$data->class}}</th>
            <th>{{$month}}</th>
            <th>{{$get_fee->class_fee}}</th>
            <th>{{isset($data->transport_fee)?$data->transport_fee:''}}</th>
            <th>@if(isset($data->fee_discount)){{$data->fee_discount}}% @endif</th>
            @php
            $total_fee=$get_fee->class_fee+$data->transport_fee;
            $discounted_fee=($total_fee*$data->fee_discount)/100;
            $total_fee=$total_fee- $discounted_fee;
            @endphp
            <th>{{isset($discounted_fee)?$discounted_fee:''}}</th>
            <th>{{ $total_fee}}</th>
        </tr>
    </tbody>
</table>
@if(!isset($footer_not) || $footer_not == 0)
</div>
</div>

</body>

</html>
@endif
