<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMonthDownload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('downloads', function (Blueprint $table) {
            $table->date('downloads_challan_month')->nullable();
            $table->string('student_class')->nullable();
            $table->tinyInteger('challan_genrated')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('downloads', function (Blueprint $table) {
            $table->dropColumn('downloads_challan_month');
            $table->dropColumn('challan_genrated');
            $table->dropColumn('student_class');
        });
    }
}
