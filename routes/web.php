<?php

use Illuminate\Support\Facades\Route;

Route::get("/", function(){
    return redirect('admin/login');
 });

//Route::get('/','Web\HomeController@home');

Route::get('/categories/{category_slug}','Web\HomeController@getCategories');
Route::get('/authors/{authors}','Web\HomeController@getauthor');
Route::get('/categories','Web\HomeController@Categories');
Route::get('/authors','Web\HomeController@authors');

Route::get('/search','Web\HomeController@search');


Route::post('/showquote','Web\HomeController@showquote');

Route::get('/quotes/{id}','Web\HomeController@getquote');

