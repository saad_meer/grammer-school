<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Pages;
use Validator;
class PagesController extends Controller
{
    public function index()
    {
        return view('pages.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'page_name',
            1 =>'page_status',
            2 =>'created_at',
            3=> 'id',
        );

            $totalData = Pages::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $page = Pages::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $page =  Pages::where('page_name','LIKE',"%{$search}%")
                        ->orWhere('page_status', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered =  Pages::where('page_name','LIKE',"%{$search}%")
                                ->orWhere('page_status', 'LIKE',"%{$search}%")
                                ->count();
            }

            $data = array();
            if(!empty($page))
            {

            foreach ($page as $value)
            {
            $edit =  url('/admin/pages/edit',$value->id);

            $nestedData['name'] = $value->page_name;
            $nestedData['status'] = $value->page_status;
            $nestedData['created_at'] = format_date_time($value->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('pages.add');
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_name' => 'required',
        ],
        [
            'page_name.required' => 'Tiltle is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $page= new Pages();
            $page->page_slug=createSlug($request->page_name);
            $page->page_name=$request->page_name;
            $page->page_description=$request->page_description;
            $page->page_status=$request->page_status;
            $page->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);
        }
    }
    public function edit($id)
    {
        $page=Pages::where('id','=',$id)->first();
        if($page)
        {
            return view('pages.edit',compact('page'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $page=Pages::where('id','=',$request->id)->first();
        if($page)
        {
            $validator = Validator::make($request->all(), [
                'page_name' => 'required',
            ],
            [
                'page_name.required' => 'Tiltle is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $page= new Pages();
                $page->page_slug=createSlug($request->page_name);
                $page->page_name=$request->page_name;
                $page->page_description=$request->page_description;
                $page->page_status=$request->page_status;
                $page->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);
            }


        }
        return redirect('admin/pages/display');

    }
    public function delete($id)
    {
        $delete=Pages::where('id','=',$id)->first();
        if($delete)
        {
            $delete->delete();
            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
