
@extends('layouts.header')
@section('title','Add Student')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Export Student List</h4>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Genrate Student List" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/reports/student_list_genrate')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Please Select Class</label>
                                        <select class="custom-select student_class" name="student_class">
                                            <option value="">Please Select Class</option>
                                            @foreach ($student_class as $student)
                                                <option value="{{$student->student_class}}">{{$student->student_class}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Please Select Status</label>
                                        <select class="custom-select student_status" name="status">
                                            <option value="">Please Select Status</option>
                                            <option value="Active">Active</option>
                                            <option value="InActive">InActive</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <button class="btn btn-primary float-right mb-2" onclick="genrateexcel()">Genrate Excel Report</button>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead class="thead-primary">
                                            <th>Registration No.</th>
                                            <th>Student Name</th>
                                            <th>Father Name</th>
                                            <th>Class</th>
                                            <th>Gender</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody id="student_record">
                                            <tr>
                                                <td scope="row" class="text-center" colspan="7">No record</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">

function genrateexcel()
{

    var url = '{{url("/")}}';
    var std_class=$('.student_class option:selected').val();
    if(std_class==NaN || std_class=='')
    {
        swalerror('Please Select Student Class');
        return false;
    }

    window.location.href=url+'/admin/reports/studentexcelexport?status='+status+'&std_class='+std_class;


}


$('#addform').submit(function(event) {
    var url = '{{url("/")}}';


//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {

            if(data.html=='')
            {
                $('#student_record').html('<tr><td scope="row" class="text-center" colspan="9">Challan Already Created</td></tr>');
            }
            else
            {
                $('#student_record').html(data.html);
            }

        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



