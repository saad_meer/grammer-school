<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FeeStructure extends Model
{
    public $table = 'fee_structure';
}
