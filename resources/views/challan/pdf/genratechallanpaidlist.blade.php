<!DOCTYPE html>
<html>
<head>
  <title>Challan Report</title>
  <style type="text/css">

    table{
        font-size: x-small;
        border-collapse: collapse;
    }
    tr td{
        font-weight: bold;
        font-size: x-small;
    }

    .gray {
        background-color: lightgray
    }
    #customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}



#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #04AA6D;
  color: white;
}
.center {
  display: block;
  margin-left: 42%;
  margin-right: 50%;
}
  </style>
</head>
<body>

<img src="{{asset('images/logo.png')}}" class="center" width="120"  valign="center">
<h4  style="text-align: center;padding-top:3px;padding-bottom:3px;">Challan Report</h4>
<table id="customers">
  <tr>
        <th>Sr. no</th>
        <th>Student Name</th>
        <th>Father Name</th>
        <th>Class</th>
        <th>Fee Month</th>
        <th>Actual Fee</th>
        <th>Transport</th>
        <th>Discount</th>
        <th>Discount Fee</th>
        <th>Late Fee</th>
        <th>Total</th>
        <th>Status</th>
  </tr>

  @if (isset($challan))
  @php
      $count=1;
  @endphp
        @foreach ($challan as $key=>$challandata)
                <tr class="gray">
                    <td colspan="12">{{date('d-m-Y',strtotime($key))}}</td>
                </tr>
                @foreach ($challandata as $data)
                    <tr>
                        <td>{{$count}}</td>
                        <td>{{$data->student_name}}</td>
                        <td>{{$data->father_name}}</td>
                        <td>{{$data->class}}</td>
                        <td>{{date('M',strtotime($data->challan_month))}}</td>
                        <td>{{$data->original_amount}}</td>
                        <td>{{isset($data->transport_fee)?$data->transport_fee:''}}</td>
                        <td>@if(isset($data->fee_discount)){{$data->fee_discount}}% @endif</td>
                        <td>{{$data->discount_amount}}</td>
                        <td>{{$data->late_fee_amount}}</td>
                        <td>{{$data->amount_paid}}</td>
                        <td>{{$data->challan_status}}</td>
                    </tr>
                    @php
      $count=$count+1;
  @endphp
                @endforeach
        @endforeach
  @endif

</table>

</body>
</html>
