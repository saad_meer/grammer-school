<table>
    <thead>
        <tr>
            <th>Registration No.</th>
            <th>Student Name</th>
            <th>Father Name</th>
            <th>Class</th>
            <th>Gender</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @if (isset($student))
            @foreach ($student as $value)
                <tr>
                    <td>{{$value->registration_number}}</td>
                    <td>{{$value->student_name}}</td>
                    <td>{{$value->father_name}}</td>
                    <td>{{$value->class}}</td>
                    <td>{{$value->gender}}</td>
                    <td>{{$value->student_status}}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
