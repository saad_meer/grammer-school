
@extends('layouts.header')
@section('title','Update Challan')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Update Challan</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/challan/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Add Challan" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/challan/updatechallan')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Please Select Class</label>
                                        <select class="custom-select student_class" name="student_class">
                                            <option value="">Please Select Class</option>
                                            @foreach ($student_class as $student)
                                                <option value="{{$student->student_class}}">{{$student->student_class}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Please Select Student</label>
                                        <select class="custom-select " name="student" id="student">
                                            <option value="">Please Select Student</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Please Select Month</label>
                                    <input type="text" class="form-control" autocomplete="off" placeholder="Choose date" name="challan_month" id="challan_month">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <form id="" action="{{url('admin/challan/updatechallanProcess')}}" method="POST">
                                    @csrf
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead class="thead-primary">
                                                <th>Challan Number</th>
                                                <th>Name</th>
                                                <th>Month</th>
                                                <th>Amount</th>
                                                <th>Miscellaneous</th>
                                                <th>Transport</th>
                                                <th>Discount</th>
                                                <th>Total</th>
                                                <th>Late Fee Amount</th>
                                                <th>Status</th>
                                            </thead>
                                            <tbody id="student_record">
                                                <tr>
                                                    <td scope="row" class="text-center" colspan="9">No record</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                <button id="update_challan" class="btn btn-primary float-right mb-2 d-none"  type="submit" >Update Challan</button>

                                </form>
                            </div>
                        </div>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">

function genratechallan()
{

    var url = '{{url("/")}}';
    var std_class=$('.student_class option:selected').val();
    if(std_class==NaN || std_class=='')
    {
        swalerror('Please Select Student Class');
        return false;
    }
    var student=$('#student option:selected').val();

    if(student==NaN || student=='')
    {
        swalerror('Please Select Student');
        return false;
    }
    var challan_month=$('#challan_month').val();
    if(challan_month==NaN || challan_month=='')
    {
        swalerror('Please Select  Month');
        return false;
    }


    $.ajax({
    url: url+'/admin/challan/genratechallan',
    type: 'POST',
    data: {std_class:std_class,student:student,challan_month:challan_month},
    success: function (data) {
        if(data.code==200)
        {
            window.open(data.path, '_blank').focus();
            swalsuccess(data.message);

        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});

}
$(function(){
        'use strict'
        $('#challan_month').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
    });
$(".student_class").change(function()
{
    $('#update_challan').addClass('d-none');

    var html='';
    var url = '{{url("/")}}';
    var student_class = $('.student_class').val();
    $.ajax({
        url: url+'/admin/get_student_class',
        type: 'POST',
        data: {
            student_class: student_class
        },
        success: function (data) {
            if(data.code==200)
            {
                html+='<option value="All">All Students</option>';
                $.each( data.result, function( key, value ) {
                    html+='<option value="'+value.id+'">'+value.student_name+'</option>';
                });
                console.log(html);
                $('#student').html(html);
            }
            if(data.code==404)
            {
                html='<option value="">Please Select Student</option>';
                $('#student').html(html);
            }
        },
        error: function (error) {
        // swalerror("SomeThing Went Wrong");
        }
    });
});

$('#addform').submit(function(event) {
    var url = '{{url("/")}}';


//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#datepicker_challan').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
            if(data.html=='')
            {
                $('#student_record').html('<tr><td scope="row" class="text-center" colspan="9">Challan Already Updated</td></tr>');
            }
            else
            {
                $('#student_record').html(data.html);
                $('#update_challan').removeClass('d-none');
            }

        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



