<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Motivation Quotes</title>
  <link rel="stylesheet" href="{{url('webcss/css/bootstrap-icons.css')}}">
  <link rel="stylesheet" href="{{url('use.fontawesome.com/releases/v5.7.0/css/all.css')}}">
  <link rel="stylesheet" href="{{url('webcss/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('webcss/css/mdb.min.css')}}">
  <link rel="stylesheet" href="{{url('webcss/css/customstyle.css')}}">
  <meta property="og:url"           content="{{url('/')}}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Motivation Quotes" />
  <meta property="og:description"   content="Motivation Quotes" />
  <meta property="og:image"         content="{{url('webcss/img/logo/logo.png')}}" />
  <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v10.0" nonce="pB3c51Br"></script>
</head>
<body>

     <!-- ========================= header section ========================== -->
  <header>
    <!--Navbar -->
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark light-color py-lg-0 pr-lg-0 fixed-top">
      <a class="navbar-brand" href="#"><img src="{{url('webcss/img/logo/logo.png')}}" alt=""></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="modal" data-bs-target="#categories">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarSupportedContent-333">
      <ul class="navbar-nav mr-auto ml-lg-3 py-lg-0">

        <li class="nav-item  {{ Request::is('/') ? 'active' : '' }} ">
          <a class="nav-link text-grey " href="{{url('/')}}">Home
            <span class="sr-only">(current)</span>
          </a>
        </li>

        <li class="nav-item  {{ Request::is('categories') ? 'active' : '' }}">
          <a class="nav-link text-grey" href="{{url('/categories')}}">Categories</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-grey" href="#">Quotes Of The Day</a>
        </li>
        <li class="nav-item  {{ Request::is('authors') ? 'active' : '' }}">
          <a class="nav-link text-grey" href="{{url('/authors')}}">Authors</a>
        </li>
      </ul>
      <ul class="navbar-nav ms-auto nav-flex-icons">
        <li class="nav-item">
          <a class="nav-link ">
            <i class="fab fa-twitter text-grey"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link ">
            <i class="fab fa-google-plus-g text-grey"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link ">
            <i class="fab fa-instagram text-grey"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link ">
            <i class="fab fa-facebook-f text-grey"></i>
          </a>
        </li>
        <?php /*
        <li class="nav-item dropdown bg-info">
          <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-key"></i>
          login/Register
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-primary"
        aria-labelledby="navbarDropdownMenuLink-333">
        <a class="dropdown-item reg-link" href="#" data-id="#pills-login" data-bs-toggle="modal"   data-bs-target="#Loginmodal">Login</a>
        <a class="dropdown-item reg-link" href="#" data-id="#pills-register" data-bs-toggle="modal"   data-bs-target="#Loginmodal">Registration</a>
      </div>
    </li>
    */?>
  </ul>
</div>
</nav>
<!--/.Navbar -->

</header>

   <!-- ========================= banner section ========================== -->
<div id="home" class="view jarallax " data-jarallax='{"speed": 0.2}' style="background-image: url('webcss/img/banner/topbanner.png'); background-repeat: no-repeat; background-size: 100% 100%; background-position: center center;">
  <div class="rgba-black-slight h-100 d-flex  justify-content-center align-items-center" style="box-shadow: 0px;">
    <div class="container mt-5">
      <div class="row ">
        <div class="col-lg-10 mx-auto wow fadeInUp slow">
          <h1 class="h1-responsive font-weight-bold white-text banner-heading text-uppercase text-center">Search <span>Quotes</span> on Any Topic</h1>

        </div>

        <div class="col-lg-8  mx-auto wow fadeInUp slow">
          <p class="banner-leading-text lead text-center">You can find all categories of quotes at The Motivation Quotes </p>
          <div class="d-flex text-center  mt-3 w-100 searchbar "  style="border-radius: 25px;border:1px solid white">
            <form action="{{url('/search')}}" method="get" style="display:block;width:100%;;">
                <input type="text" name="search" @if(isset($_GET['search'])) value="{{$_GET['search']}}" @endif class="form-control text-blue banner-input  border-0   form-control-lg " placeholder="Enter Keyword..">
                <button class="btn  text-blue mt-0 banner-btn     pl-1   m-0 border-0  z-depth-0" ><i class="fa fa-search"></i></button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 <!-- ========================= Main  section ========================== -->
<main>
    @yield('content')
</main>
<!-- Modal -->
<div class="modal fade" id="detailmodel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md  modal-dialog-centered modal-fullscreen-md-down" >
    <div class="modal-content" style="background-color: #00000000;box-shadow:none;">

      <div class="modal-body" id="content_quote" style="min-height: 250px;">
      </div>


    </div>
  </div>
</div>
@include('web.layouts.footer')
