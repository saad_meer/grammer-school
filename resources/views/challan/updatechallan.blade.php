@if (isset($challan) && count($challan)>0)
    @foreach ($challan as  $data)

        <tr>
            <th>{{$data->challan_number}}</th>
            <th>{{$data->student_name}}</th>
            <th>{{date('M Y',strtotime($data->challan_month))}}</th>
            <th>{{$data->original_amount}}</th>
            <th>{{$data->miscellaneous}}</th>
            <th>{{isset($data->transport_fee)?$data->transport_fee:'-'}}</th>
            <th>{{isset($data->discount_amount)?$data->discount_amount:'-'}}</th>
            <th>{{$data->amount_paid}}</th>
            <th><input type="number" class="form-control" min="0" step="0.01" name="data[{{$data->challan_number}}][amount]"></th>
            <th><select class="form-control" name="data[{{$data->challan_number}}][status]" id=""><option value="Unpaid">UnPaid</option><option value="Paid" selected>Paid</option></select></th>
        </tr>
    @endforeach
@endif

