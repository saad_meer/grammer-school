<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes([
  'register' => false, // Registration Routes...
  'reset' => false, // Password Reset Routes...
  'verify' => false, // Email Verification Routes...
]);
Route::get('/admin/login','Auth\LoginController@login');

Route::post('/admin/login','Auth\LoginController@loginProcess');
Route::post('/admin/logout','Auth\LoginController@logout');

// Route::get('/register','Auth\LoginController@signup');
// Route::post('/register','Auth\LoginController@signupProcess');

Route::get('/admin/dashboard', 'HomeController@index')->middleware('auth','validateuser');
Route::get('refresh-csrf', function(){
    return csrf_token();
});
Route::group(['middleware' => 'auth','namespace' => 'Admin','prefix' => 'admin'], function () {

    Route::post('/get_student_class','ChallanController@getstudent');

    Route::get('/useractivity','StudentsController@useractivity');
    Route::post('/useractivitydisplay','StudentsController@useractivitydisplay');

    Route::get('/users/editprofile', 'UsersController@profile');
    Route::post('/users/editprofileProcess', 'UsersController@profileProcess');

    Route::group(['middleware' => 'validateuser','prefix' => 'users',], function () {
        // Route::get('/editprofile', 'UsersController@profile');
        // Route::post('/editprofileProcess', 'UsersController@profileProcess');

        Route::get('/display', 'UsersController@index');
        Route::post('/display', 'UsersController@display');
        Route::get('/add', 'UsersController@add');
        Route::post('/addProcess', 'UsersController@addProcess');
        Route::get('/edit/{id}', 'UsersController@edit');
        Route::post('/editProcess', 'UsersController@editProcess');
        Route::get('/delete/{id}', 'UsersController@delete');
    });

    Route::group(['prefix' => 'students',], function () {
        Route::get('/display', 'StudentsController@index');
        Route::post('/display', 'StudentsController@display');
        Route::get('/add', 'StudentsController@add');
        Route::post('/addProcess', 'StudentsController@addProcess');
        Route::get('/edit/{id}', 'StudentsController@edit');
        Route::post('/editProcess', 'StudentsController@editProcess');
        Route::get('/delete/{id}', 'StudentsController@delete')->middleware('validateuser');

        Route::get('/upgrade', 'StudentsController@upgrade');
        Route::post('/promote', 'StudentsController@promote');

      //  Route::get('/import_student', 'StudentsController@import_student');
    });


    Route::group(['prefix' => 'challan',], function () {
        Route::get('/view/{id}', 'ChallanController@show');
        Route::get('/display', 'ChallanController@index');
        Route::post('/display', 'ChallanController@display');
        Route::get('/add', 'ChallanController@add');
        Route::post('/addProcess', 'ChallanController@addProcess');
        Route::get('/update', 'ChallanController@update');
        Route::post('/updatechallan', 'ChallanController@updatechallan');
        Route::post('/updatechallanProcess', 'ChallanController@updatechallanProcess');
        Route::get('/delete/{id}', 'ChallanController@delete')->middleware('validateuser');
        Route::post('/genratechallan','ChallanController@genratechallan');

        Route::get('/downloaddisplay','ChallanController@challandownloaddisplay');
        Route::post('/challandownload','ChallanController@challandownload');

    });
    Route::group(['prefix' => 'reports',], function () {
        Route::get('/student_list', 'ReportController@studentlist');
        Route::post('/student_list_genrate', 'ReportController@getlist');
        Route::get('/studentexcelexport', 'ReportController@studentexcelexport');

        Route::get('/challanlist', 'ReportController@challanlist');
        Route::get('/challanlistexport', 'ReportController@challanlistexport');

    });
    Route::group(['middleware' => 'validateuser','prefix' => 'settings',], function () {
        Route::get('/add', 'SettingController@setting');
        Route::post('/addsettings', 'SettingController@addsetting');
    });

    Route::group(['prefix' => 'pages',], function () {
        Route::get('/display', 'PagesController@index');
        Route::post('/display', 'PagesController@display');
        Route::get('/add', 'PagesController@add');
        Route::post('/addProcess', 'PagesController@addProcess');
        Route::get('/edit/{id}', 'PagesController@edit');
        Route::post('/editProcess', 'PagesController@editProcess');
        Route::get('/delete/{id}', 'PagesController@delete');
    });

    Route::get('/genrate_challan/{id}', 'ReportController@genratereport');

});
