<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Student;
use App\Models\Admin\Challan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_student=Student::count();
        $active_student=Student::where('student_status','Active')->count();
        $inactive_student=Student::where('student_status','Inactive')->count();

        $total_challan=Challan::whereMonth('challan_month',date('m'))->count();
        $paid_challan=Challan::whereMonth('challan_month',date('m'))->where('challan_status','Paid')->count();
        $unpaid_challan=Challan::whereMonth('challan_month',date('m'))->where('challan_status','Unpaid')->count();

        $challan_list=Challan::whereMonth('challan_month',date('m'))->where('challan_status','Paid')->latest()->take(5)->get();


        return view('home',compact('total_student','active_student','inactive_student','total_challan','paid_challan','unpaid_challan','challan_list'));
    }
}
