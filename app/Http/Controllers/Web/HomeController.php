<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\Models\Admin\SubCategory;
use App\Models\Admin\Author;
use App\Models\Admin\Quote;
use View;
class HomeController extends Controller
{
    public function home()
    {
        $category=Category::where('category_status','Active')->get();
        $categoryArray=[];
        foreach($category as $key=>$value)
        {
            $SubCategory=SubCategory::where('category_id',$value->category_id)->get();
            $categoryArray[]=$value;
            if(isset( $SubCategory) &&  count($SubCategory)>0)
            {
                $categoryArray[$key]['subcategory']=$SubCategory;
            }

        }
        $result['quote']=Quote::where('quotes_status','Active')
                    ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
                    ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
                    ->paginate(20);

        $result['categoryArray']= $categoryArray;

        return view('web.home',compact('result'));
    }
    public function getCategories($category_slug)
    {

        $category=Category::where('category_status','Active')->get();
        $categoryArray=[];
        foreach($category as $key=>$value)
        {
            $SubCategory=SubCategory::where('category_id',$value->category_id)->get();
            $categoryArray[]=$value;
            if(isset( $SubCategory) &&  count($SubCategory)>0)
            {
                $categoryArray[$key]['subcategory']=$SubCategory;
            }

        }

        $result['categoryArray']= $categoryArray;
        $category_search=Category::where('category_status','Active')->where('category_slug',$category_slug)->first();
        $subcategory_search=SubCategory::where('subcategory_status','Active')->where('sub_category_slug',$category_slug)->first();

        if(isset($category_search))
        {

            $result['quote']=Quote::where('quotes_status','Active')
            ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
            ->whereRaw("find_in_set('".$category_search->category_id."',quotes.quote_category)")
            ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
            ->paginate(20);

            return view('web.home',compact('result'));
        }
        elseif($subcategory_search)
        {

            $result['quote']=Quote::where('quotes_status','Active')
            ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
            ->whereRaw("find_in_set('".$subcategory_search->id."',quotes.quote_category)")
            ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
            ->paginate(20);
            return view('web.home',compact('result'));

        }
        return view('web.category',compact('result'));
    }
    public function getauthor($authors)
    {
        if($authors)
        {
            $category=Category::where('category_status','Active')->get();
            $categoryArray=[];
            foreach($category as $key=>$value)
            {
                $SubCategory=SubCategory::where('category_id',$value->category_id)->get();
                $categoryArray[]=$value;
                if(isset( $SubCategory) &&  count($SubCategory)>0)
                {
                    $categoryArray[$key]['subcategory']=$SubCategory;
                }

            }

            $result['categoryArray']= $categoryArray;
            $authors=explode('-',$authors);
            $author=Author::where('author_first_name', 'LIKE', '%'.$authors[0].'%')
                                ->where('author_last_name', 'LIKE', '%'.$authors[1].'%')
                                ->first();
            if($author)
            {
                $result['quote']=Quote::where('quotes_status','Active')
                ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
                ->where('quotes.author_id',$author->id)
                ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
                ->paginate(20);
                return view('web.home',compact('result'));

            }
        }

        return redirect('/')->back();
    }
    public function authors()
    {
        $result['author']=Author::where('author_status','Active')->get();

        return view('web.author',compact('result'));
    }
    public function categories()
    {
        $result['category']=Category::where('category_status','Active')->get();

        return view('web.author',compact('result'));

    }
    public function search(Request $request)
    {
        $category=Category::where('category_status','Active')->get();
        $categoryArray=[];
        foreach($category as $key=>$value)
        {
            $SubCategory=SubCategory::where('category_id',$value->category_id)->get();
            $categoryArray[]=$value;
            if(isset( $SubCategory) &&  count($SubCategory)>0)
            {
                $categoryArray[$key]['subcategory']=$SubCategory;
            }

        }

        $result['categoryArray']= $categoryArray;
        $search=$request->search;
        if(!empty($search))
        {
            $category_search=Category::where('category_name', 'LIKE', '%'.$search.'%')->first();
            if(isset($category_search))
            {
                $result['quote']=Quote::where('quotes_status','Active')
                ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
                ->whereRaw("find_in_set('".$category_search->category_id."',quotes.quote_category)")
                ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
                ->paginate(20);
                return view('web.home',compact('result'));
            }
            $subcategory_search=SubCategory::where('subcategory_name', 'LIKE', '%'.$search.'%')->first();
            if(isset($subcategory_search))
            {

                $result['quote']=Quote::where('quotes_status','Active')
                ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
                ->whereRaw("find_in_set('".$subcategory_search->id."',quotes.quote_category)")
                ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
                ->paginate(20);
                return view('web.home',compact('result'));
            }
            return view('web.home',compact('result'));
        }
    }
    public function showquote(Request $request)
    {
        $id=$request->id;
        if($id>0)
        {
            $quote=Quote::where('quotes_status','Active')
            ->leftjoin('quoto_template','quotes.template_id','quoto_template.id')
            ->where('quotes.id',$id)
            ->select('quoto_template.*','quotes.*','quotes.id as quotes_id')
            ->first();
            if($quote)
            {
                $view = View::make('web.quote-template', ['quote' => $quote,'column_check'=>1])->render();
                return response()->json(['code'=>200,'html'=>$view,'quote'=>$quote]);
            }
            return response()->json(['code'=>404]);

        }
        else
        {
            return response()->json(['code'=>404]);
        }

    }
    public function getquote($id)
    {
        if($id>0)
        {
            return redirect('/')->with('model_popup',$id);
        }
    }
}
