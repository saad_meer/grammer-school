 <!-- ========================= Footer Section ========================== -->
 <footer class="footer bg-dark">
    <div class="first-body py-5">
      <div class="container container-xl">
        <div class="row gy-5 justify-content-between">
          <div class="col-xxl-4 col-lg-5 col-md-12">
            <img src="webcss/img/logo/footer-logo.png" alt="">
            <p class="about mt-4">Lorem ipsum dolor sit amet, consectetur adipisic ing elit, sed
               do eiusmod tempor incididunt ut lab ore et dolore magna aliqua. Ut enim ad minim
               ven iam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehend</p>
          </div>
          <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-6">
            <h3 class="footer-links-heading">Quick Links</h3>
             <div class="d-sm-flex flex-wrap justify-content-between">
                <ul class="mt-4 footer-links list-unstyled mb-0 mb-mb-3">
                    @php $get_pages=get_pages();@endphp
                    @if(isset($get_pages))
                        @foreach ($get_pages as $pages)
                            <li><a href="javascript:void(0)">{{$pages->page_name}}</a></li>
                        @endforeach
                    @endif
                </ul>
                <ul class="mt-md-4 footer-links list-unstyled">
                  <li><a href="{{url('categories')}}">Quote‘s Categories</a></li>
                  <li><a href="javascript:void(0)">Quotes Of The Day</a></li>
                  <li><a href="{{url('authors')}}">All Authors</a></li>
                  <li><a href="javascript:void(0)">Login/Register</a></li>
                </ul>
             </div>
          </div>

          <div class="col-xxl-3 col-xl-3 col-lg-6 col-md-5 col-sm-5">
            <h3 class="footer-socials-heading " style="margin-left: 16px;">Social Media</h3>
            <ul class="mt-4 footer-social-links list-unstyled">
              <li><a href="javascript:void(0)"><i class="fab fa-facebook-f"></i> &nbsp;Follow Us On Facebook</a></li>
              <li><a href="javascript:void(0)"><i class="fab fa-google-plus-g"></i> &nbsp;Follow Us On Google</a></li>
              <li><a href="javascript:void(0)"><i class="fab fa-instagram"></i> &nbsp;Follow Us On Instagram</a></li>
              <li><a href="javascript:void(0)"><i class="fab fa-twitter"></i> &nbsp;Follow Us On Instagram</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="second-body d-flex justify-content-center align-items-center">
        <p class="m-0">The Motivation Quotes  © 2019 / All copy rights reserved</p>
    </div>
  </footer>



   <!-- ========================= Footer Section ========================== -->
 <!-- ========================= Mobile categories Modal ========================== -->
<!-- Modal -->
<div class="modal fade left" id="categories" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen-lg-down ">
      <div class="modal-content">
        <div class="modal-header">
          <div class="btn-group">
            <a href="javascript:void(0)" class="btn btn-primary border-blue btn-sm reg-link"  data-id="#pills-login" data-bs-toggle="modal"   data-bs-target="#Loginmodal">Login</a>
            &nbsp;
            <a href="javascript:void(0)" class="btn btn-primary border-blue btn-sm reg-link" data-bs-toggle="modal" data-id="#pills-register" data-bs-target="#Loginmodal">Register</a>
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <ul class="list-group list-group-flush mobile_menu">
            <li class="list-group-item ps-0">
              <a class="nav-link  ps-0" href="index.html"><i class="fas fa-home" aria-hidden="true"></i> &nbsp;Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="list-group-item ps-0">
              <a class="nav-link ps-0" href="#"><i class="fas fa-boxes" aria-hidden="true"></i> &nbsp; Categories</a>
            </li>
            <li class="list-group-item ps-0">
              <a class="nav-link  ps-0" href="#"><i class="fas fa-calendar-day" aria-hidden="true"></i> &nbsp; Quotes Of The Day</a>
            </li>
            <li class="list-group-item ps-0">
              <a class="nav-link  ps-0" href="#"><i class="fas fa-signature" aria-hidden="true"></i> &nbsp; Authors</a>
            </li>
          </ul>
          <div class="d-flex mb-2 mt-3">
            <h2 class="card-title right-side-card-heading ">
              <i class="fas fa-birthday-cake text-primary"></i> &nbsp;
              <span class="m-auto">Today's Birthdays</span>
            </h2>
          </div>
          <hr class="border-primary m-0">
          <!--Accordion wrapper-->
          <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

            <!-- Accordion card -->
            <div class="card border-0">

              <!-- Card header -->
              <div class="card-header" role="tab" id="headingOne1">
                <a class="collapsed d-flex" data-toggle="collapse" data-parent="#accordionEx" href="#collapsecat1"
                aria-expanded="false" aria-controls="collapsecat1" class="d-flex ">
                  <h5 class="m-0 toggle-heading">
                    <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Martha Stewart
                  </h5>
                  <h5 class="m-0 toggle-heading ms-auto">(300)</h5>
                </a>
              </div>

              <!-- Card body -->
              <div id="collapsecat1" class="collapse " role="tabpanel" aria-labelledby="headingOne1"
                data-parent="#accordionEx">
                <div class="card-body py-0">
                  <ul class="list-unstyled list-group-flush">
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>

                  </ul>
                </div>
              </div>

            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="card border-0">

              <!-- Card header -->
              <div class="card-header" role="tab" id="headingTwo2">
                <a class="collapsed d-flex" data-toggle="collapse" data-parent="#accordionEx" href="#collapsecat2"
                  aria-expanded="false" aria-controls="collapsecat2">
                  <h5 class="m-0 toggle-heading">
                    <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Ernie Pyle
                  </h5>
                  <h5 class="m-0 toggle-heading ms-auto">(300)</h5>
                </a>
              </div>

              <!-- Card body -->
              <div id="collapsecat2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                data-parent="#accordionEx">
                <div class="card-body py-0 ">
                  <ul class="list-unstyled list-group-flush">
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>

                  </ul>
                </div>
              </div>

            </div>
            <!-- Accordion card -->

            <!-- Accordion card -->
            <div class="card border-0">

              <!-- Card header -->
              <div class="card-header" role="tab" id="headingThree3">
                <a class="collapsed d-flex" data-toggle="collapse" data-parent="#accordionEx" href="#collapsecat3"
                  aria-expanded="false" aria-controls="collapsecat3">
                  <h5 class="m-0 toggle-heading">
                    <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Martin Sheen
                  </h5>
                  <h5 class="m-0 toggle-heading ms-auto">(300)</h5>
                </a>
              </div>

              <!-- Card body -->
              <div id="collapsecat3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                data-parent="#accordionEx">
                <div class="card-body py-0">
                  <ul class="list-unstyled list-group-flush">
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>
                    <li class="list-group-item">Name 1</li>

                  </ul>
                </div>
              </div>

            </div>
            <!-- Accordion card -->
            <div class="my-2">
              <a href="javascript:void(0)" class="viewall">View All &nbsp; <i class="fas fa-arrow-right"></i> </a>
            </div>
          </div>
          <!-- Accordion wrapper -->
               <hr class="m-0">
                <div class="d-flex mb-2 mt-3">
                  <h2 class="card-title right-side-card-heading ">
                    <i class="fas fa-boxes text-primary"></i> &nbsp;
                    <span class="m-auto">Categories</span>
                  </h2>
                </div>
                <hr class="m-0">
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingOne1">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1"
                      aria-expanded="false" aria-controls="collapseTwo1">
                        <h5 class="m-0 toggle-heading">
                          <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Motivational
                        </h5>
                      </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseOne1" class="collapse " role="tabpanel" aria-labelledby="headingOne1"
                      data-parent="#accordionEx">
                      <div class="card-body py-0">
                        <ul class="list-unstyled list-group-flush">
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>

                        </ul>
                      </div>
                    </div>

                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingTwo2">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
                        aria-expanded="false" aria-controls="collapseTwo2">
                        <h5 class="m-0 toggle-heading">
                          <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Funny
                        </h5>
                      </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
                      data-parent="#accordionEx">
                      <div class="card-body py-0 ">
                        <ul class="list-unstyled list-group-flush">
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>

                        </ul>
                      </div>
                    </div>

                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingThree3">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
                        aria-expanded="false" aria-controls="collapseThree3">
                        <h5 class="m-0 toggle-heading">
                          <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Nature
                        </h5>
                      </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
                      data-parent="#accordionEx">
                      <div class="card-body py-0">
                        <ul class="list-unstyled list-group-flush">
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>

                        </ul>

                      </div>
                    </div>

                  </div>
                  <!-- Accordion card -->
                  <div class="my-2">
                    <a href="javascript:void(0)" class="viewall">View All &nbsp; <i class="fas fa-arrow-right"></i> </a>
                  </div>
                </div>
                <!-- Accordion wrapper -->

                <hr class="m-0">
                <div class="d-flex mb-2 mt-3">
                  <h2 class="card-title right-side-card-heading ">
                    <i class="fas fa-user text-primary"></i> &nbsp;
                    <span class="m-auto">Authors</span>
                  </h2>
                </div>
                <hr class="m-0">
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">

                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingfour1">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapsefour1"
                      aria-expanded="false" aria-controls="collapseTwo1">
                        <h5 class="m-0 toggle-heading">
                          <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Zack Greinke
                        </h5>
                      </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapsefour1" class="collapse " role="tabpanel" aria-labelledby="headingfour1"
                      data-parent="#accordionEx">
                      <div class="card-body py-0">
                        <ul class="list-unstyled list-group-flush">
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>

                        </ul>
                      </div>
                    </div>

                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingfour2">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapsefour2"
                        aria-expanded="false" aria-controls="collapsefour2">
                        <h5 class="m-0 toggle-heading">
                          <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Helen Keller
                        </h5>
                      </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapsefour2" class="collapse" role="tabpanel" aria-labelledby="headingfour2"
                      data-parent="#accordionEx">
                      <div class="card-body py-0 ">
                        <ul class="list-unstyled list-group-flush">
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>

                        </ul>
                      </div>
                    </div>

                  </div>
                  <!-- Accordion card -->

                  <!-- Accordion card -->
                  <div class="card border-0">

                    <!-- Card header -->
                    <div class="card-header" role="tab" id="headingfour4">
                      <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapsefour4"
                        aria-expanded="false" aria-controls="collapsefour4">
                        <h5 class="m-0 toggle-heading">
                          <i class="fas fa-angle-right rotate-icon"></i> &nbsp; Mark Twain
                        </h5>
                      </a>
                    </div>

                    <!-- Card body -->
                    <div id="collapsefour4" class="collapse" role="tabpanel" aria-labelledby="headingfour4"
                      data-parent="#accordionEx">
                      <div class="card-body py-0">
                        <ul class="list-unstyled list-group-flush">
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>
                          <li class="list-group-item">Name 1</li>

                        </ul>

                      </div>
                    </div>

                  </div>
                  <!-- Accordion card -->
                  <div class="my-2">
                    <a href="javascript:void(0)" class="viewall">View All &nbsp; <i class="fas fa-arrow-right"></i> </a>
                  </div>
                </div>
                <!-- Accordion wrapper -->
        </div>
        <div class="modal-footer justify-content-between list-unstyled p-0">
            <li class="nav-item">
              <a class="nav-link ">
                <i class="fab fa-twitter text-grey"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link ">
                <i class="fab fa-google-plus-g text-grey"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link ">
                <i class="fab fa-instagram text-grey"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link ">
                <i class="fab fa-facebook-f text-grey"></i>
              </a>
            </li>
        </div>
      </div>
    </div>
  </div>

   <!-- ========================= Login/Registeration Modal ========================== -->
  <!-- Modal -->
  <div class="modal fade right" id="Loginmodal" tabindex="-1" aria-labelledby="exampleModalLabel " aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered modal-fullscreen-lg-down">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-white fon-weight-bolder" id="exampleModalLabel">Login / Registeration</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
              <!-- Pills navs -->
            <ul class="nav nav-pills nav-justified mb-3" id="ex1" role="tablist">
              <li class="nav-item" role="presentation">
                <a
                  class="nav-link active btn-rounded"
                  id="tab-login"
                  data-bs-toggle="pill"
                  href="#pills-login"
                  role="tab"
                  aria-controls="pills-login"
                  aria-selected="true"
                  >Login</a
                >
              </li>
              <li class="nav-item" role="presentation">
                <a
                  class="nav-link btn-rounded"
                  id="tab-register"
                  data-bs-toggle="pill"
                  href="#pills-register"
                  role="tab"
                  aria-controls="pills-register"
                  aria-selected="false"
                  >Register</a
                >
              </li>
            </ul>
            <!-- Pills navs -->

            <!-- Pills content -->
            <div class="tab-content">
              <div
                class="tab-pane fade show active"
                id="pills-login"
                role="tabpanel"
                aria-labelledby="tab-login"
              >
                <form>
                  <div class="text-center mb-3">
                    <p>Sign in with:</p>
                    <button type="button" class="btn btn-primary btn-floating mx-1">
                      <i class="fab fa-facebook-f"></i>
                    </button>

                    <button type="button" class="btn btn-primary btn-floating mx-1">
                      <i class="fab fa-google"></i>
                    </button>

                    <button type="button" class="btn btn-primary btn-floating mx-1">
                      <i class="fab fa-twitter"></i>
                    </button>
                  </div>

                  <p class="text-center">or:</p>

                  <!-- Email input -->
                  <div class="md-form mb-4">
                    <input type="email" id="loginName" class="form-control" />
                    <label class="form-label" for="loginName">Email or username</label>
                  </div>

                  <!-- Password input -->
                  <div class="md-form mb-4">
                    <input type="password" id="loginPassword" class="form-control" />
                    <label class="form-label" for="loginPassword">Password</label>
                  </div>

                  <!-- 2 column grid layout -->
                  <div class="row mb-4">
                    <div class="col-md-12 d-flex justify-content-center">
                      <!-- Checkbox -->
                      <div class="form-check mb-3 mb-md-0">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          value=""
                          id="loginCheck"
                        />
                        <label class="form-check-label" for="loginCheck"> Remember me </label>
                      </div>
                    </div>

                    <div class="col-md-12 d-flex justify-content-center">
                      <!-- Simple link -->
                      <a href="#!">Forgot password?</a>
                    </div>
                  </div>

                  <!-- Submit button -->
                   <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-block mb-4 mx-auto btn-rounded">Sign in</button>
                   </div>

                  <!-- Register buttons -->
                  <div class="text-center">
                    <p>Not a member? <a href="#!" class="reg-link" data-id="#pills-register">Register</a></p>
                  </div>
                </form>
              </div>
              <div
                class="tab-pane fade"
                id="pills-register"
                role="tabpanel"
                aria-labelledby="tab-register"
              >
                <form>
                  <div class="text-center mb-3">
                    <p>Sign up with:</p>
                    <button type="button" class="btn btn-primary btn-floating mx-1">
                      <i class="fab fa-facebook-f"></i>
                    </button>

                    <button type="button" class="btn btn-primary btn-floating mx-1">
                      <i class="fab fa-google"></i>
                    </button>

                    <button type="button" class="btn btn-primary btn-floating mx-1">
                      <i class="fab fa-twitter"></i>
                    </button>
                  </div>

                  <p class="text-center">or:</p>

                  <!-- Name input -->
                  <div class="md-form mb-4">
                    <input type="text" id="registerName" class="form-control" />
                    <label class="form-label" for="registerName">Name</label>
                  </div>

                  <!-- Username input -->
                  <div class="md-form mb-4">
                    <input type="text" id="registerUsername" class="form-control" />
                    <label class="form-label" for="registerUsername">Username</label>
                  </div>

                  <!-- Email input -->
                  <div class="md-form mb-4">
                    <input type="email" id="registerEmail" class="form-control" />
                    <label class="form-label" for="registerEmail">Email</label>
                  </div>

                  <!-- Password input -->
                  <div class="md-form mb-4">
                    <input type="password" id="registerPassword" class="form-control" />
                    <label class="form-label" for="registerPassword">Password</label>
                  </div>

                  <!-- Repeat Password input -->
                  <div class="md-form  mb-4">
                    <input type="password" id="registerRepeatPassword" class="form-control" />
                    <label class="form-label" for="registerRepeatPassword">Repeat password</label>
                  </div>

                  <!-- Checkbox -->
                  <div class="form-check d-flex justify-content-center mb-4">
                    <input
                      class="form-check-input me-2"
                      type="checkbox"
                      value=""
                      id="registerCheck"

                      aria-describedby="registerCheckHelpText"
                    />
                    <label class="form-check-label" for="registerCheck">
                      I have read and agree to the terms
                    </label>
                  </div>

                   <div class="form-group text-center mt-3">
                      <!-- Submit button -->
                  <button type="submit" class="btn btn-primary btn-block mb-3 btn-rounded">Sign up</button>
                   </div>
                </form>
              </div>
            </div>
            <!-- Pills content -->
        </div>
      </div>
    </div>
  </div>


  <script src="{{url('webcss/js/jquery.min.js')}}" ></script>
  <script src="{{url('webcss/js/popper.min.js')}}" ></script>
  <script src="{{url('webcss/js/bootstrap.min.js')}}" ></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
  <script src="{{url('webcss/js/mdb.min.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>

  <script>
<?php
    if(\Session::has('model_popup') && \Session::get('model_popup') > 0){ ?>
        detailmodel(<?php echo \Session::get('model_popup');?>);
    <?php } ?>
    $(".reg-link").click(function(e){

      var regid = $(this).attr("data-id");
      $('a[href="' + regid  + '"]').tab("show");
    });
    function detailmodel(id)
    {

          var url = '{{url("/")}}';
          $.ajax({
              url: url+'/showquote',
              data: {_token: "{{ csrf_token() }}",id:id},
              type: 'POST',
              success: function (data) {
                  if(data.code==200)
                  {
                    $('#detailmodel').modal('show');
                    $('#content_quote').html(data.html);
                    var next=parseInt(data.quote['quotes_id'])+1;
                    var prev=parseInt(data.quote['quotes_id'])-1;
                    $('#content_quote').append('<a href="javascript::void(0);" onclick="detailmodel('+next+')" id="next_btn"><i class="fas fa-angle-right" style="color: white;position: absolute;left: 100%;top: 50%;font-size: 45px;"></i></a><a href="javascript::void(0);" onclick="detailmodel('+prev+')" id="prev_btn"><i class="fas fa-angle-left" style="color: white;position: absolute;top: 50%;font-size: 45px;"></i></a>');

                  }
                  if(data.code==404)
                  {
                    $('#detailmodel').modal('hide');
                  }
              },
              error: function (error) {

              }
          });

    }
  </script>
  </body>
  </html>
