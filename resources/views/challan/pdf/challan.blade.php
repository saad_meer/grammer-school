
@if(!isset($header_not) || $header_not == 0)
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE html>
<html>
<head>
  <title>Challan</title>
  <style type="text/css">

    #customers {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
    }
    #paid {
      font-family: Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      margin-top: 15px;
      width: 100%;
    }
    #paid th, #paid td
    {
      font-size: 14px;
      border: 1px dotted #ddd;

    }

#customers td, #customers th {
  border: 1px dotted #ddd;
}


.row {
    display: table;
    width: 100%; /*Optional*/
    table-layout: fixed; /*Optional*/
    border-spacing: 10px; /*Optional*/
}
.column {
    display: table-cell;
    width:35%;
    padding-right: 10px;
}
.pdf-border-div{
    border:1px solid;
    border-style: dashed;
}
.pdf-body{
  position: relative;
  width: 100%;
  margin-right: auto;
  margin-left: auto;


}
.pdf-border-div{
    border:1px solid;
    border-style: dashed;
}

  </style>
</head>
<body>
<div class="pdf-body">
    @endif
        <div class="pdf-border-div" style="page-break-after: always;">
            <div class="row">
                <div class="column">

                        <table id="customers">
                        <tr>
                                <th colspan="3" style="border:0px">The Grammar Higher Secondary School</th>
                        </tr>
                        <tr>
                            <th colspan="3"  style="border:0px;font-weight:normal;">School Copy</th>
                        </tr>
                        <tr>
                            <td  style="width: 20%;border:0px;text-align:center;"><img style="width: 70%;" src="{{url('/images/JS.jpg')}}"></td>
                            <td style="font-size:14px;text-align:center;border:0px">DINGA (GUJRAT) <br>PH# 0537-670318 </td>
                            <td style="width: 20%;border:0px;text-align:center;"><img style="width: 70%;" src="{{url('/images/logo.jpg')}}" ></td>
                        </tr>
                        <tr>
                            <th colspan="3" style="border:0px;padding-bottom: 12px;"><span style="border: 1px solid #ddd;padding: 5px;"> Fee Challan </span></th>
                        </tr>
                        <tr>
                            <td>Challan #</td>
                            <td colspan="2">{{$challan->challan_number}}</td>
                        </tr>
                        <tr>
                            <td>Name:</td>
                            <td colspan="2">{{$challan->student_name}}</td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td colspan="2">{{$challan->father_name}}</td>
                        </tr>
                        <tr>
                            <td>CNIC No Of Father/Guardian:</td>
                            <td colspan="2">{{$challan->father_cnic}}</td>
                        </tr>
                        <tr>
                            <td>Class:</td>
                            <td colspan="2">{{$challan->class}}</td>
                        </tr>
                        <tr>
                            <td>Due Date:</td>
                            <td colspan="2">{{date('d-m-Y',strtotime($challan->last_date))}}</td>
                        </tr>
                        <tr>
                            <td>Period:</td>
                            <td colspan="2">{{date('d-m-Y',strtotime($challan->challan_month))}}</td>
                        </tr>
                        </table>
                        <table id="paid" >
                        <tr>
                                <th  style="text-align: left;">Name</th>
                                <th style="text-align: right;">Amount</th>
                        </tr>
                        <tr>
                            <td  style="text-align: left;">Tution Fee</td>
                            <td style="text-align: right;">{{$challan->original_amount}}</td>
                        </tr>
                        <tr>
                        <td  style="text-align: left;">Van Rent</td>
                        <td style="text-align: right;">{{$challan->transport_fee}}</td>
                        </tr>
                        <tr>
                        <td  style="text-align: left;">Discount Fee</td>
                        <td style="text-align: right;">{{$challan->discount_amount}}</td>
                        </tr>
                        <tr>
                            <td  style="text-align: left;">Miscellaneous</td>
                            <td style="text-align: right;">{{$challan->miscellaneous}}</td>
                        </tr>
                        <tr>
                            <td  style="text-align: left;">Grand Total</td>
                            <td style="text-align: right;">{{$challan->amount_paid}}</td>
                        </tr>
                        </table>
                        <span>For bank purpose only: Transfer to Accounts</span>
                        <table id="paid" >
                        <tr>
                                <td colspan="3" style="text-align: left;border:0px;padding-right: 50px;">Challan <span style="border: 1px solid #000;padding: 5px;margin-left: 100px;">{{$challan->challan_number}}</span></td>
                        </tr>
                        <tr>
                            <td  style="text-align: left;">Credit</td>
                            <td style="text-align: left;">A/C690363</td>
                        </tr>
                        <tr>
                        <td  style="text-align: left;">Payable within due date (Rs.)</td>
                        <td style="text-align: right;">{{$challan->amount_paid}}</td>
                        </tr>
                        <tr>
                        <td  style="text-align: left;">Payable after due date (RS.)</td>
                        <td style="text-align: right;"></td>
                        </tr>
                        </table>
                        <table id="paid" style="margin-top: 17px;">
                            <tr>
                                <th style="border: 0px ">Cash Officer</th>
                                <th style="border: 0px ;">Managar Opps.</th>
                            </tr>
                        </table>
                        <table>
                            <tr style="text-align: center;">
                                <td style="font-size: 8px;text-align: center;"><strong>Note:</strong>Rs.30/- Per day will be charged after due date.</td>
                            </tr>
                            <tr style="text-align: center;">
                                <td style="font-size: 8px;text-align: center;">Rs.30/- will be charged in case of Re-issuance</td>
                            </tr>
                        </table>
                </div>
                <div class="column">

                    <table id="customers">
                    <tr>
                            <th colspan="3" style="border:0px">The Grammar Higher Secondary School</th>
                    </tr>
                    <tr>
                        <th colspan="3"  style="border:0px;font-weight:normal;">Bank Copy</th>
                    </tr>
                    <tr>
                        <td  style="width: 20%;border:0px;text-align:center;"><img style="width: 70%;" src="{{asset('JS.jpg')}}"></td>
                        <td style="font-size:14px;text-align:center;border:0px">DINGA (GUJRAT) <br>PH# 0537-670318 </td>
                        <td style="width: 20%;border:0px;text-align:center;"><img style="width: 70%;" src="{{asset('logo.jpg')}}" ></td>
                    </tr>
                    <tr>
                        <th colspan="3" style="border:0px;padding-bottom: 12px;"><span style="border: 1px solid #ddd;padding: 5px;"> Fee Challan </span></th>
                    </tr>
                    <tr>
                        <td>Challan #</td>
                        <td colspan="2">{{$challan->challan_number}}</td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td colspan="2">{{$challan->student_name}}</td>
                    </tr>
                    <tr>
                        <td>Father's Name</td>
                        <td colspan="2">{{$challan->father_name}}</td>
                    </tr>
                    <tr>
                        <td>CNIC No Of Father/Guardian:</td>
                        <td colspan="2">{{$challan->father_cnic}}</td>
                    </tr>
                    <tr>
                        <td>Class:</td>
                        <td colspan="2">{{$challan->class}}</td>
                    </tr>
                    <tr>
                        <td>Due Date:</td>
                        <td colspan="2">{{date('d-m-Y',strtotime($challan->last_date))}}</td>
                    </tr>
                    <tr>
                        <td>Period:</td>
                        <td colspan="2">{{date('d-m-Y',strtotime($challan->challan_month))}}</td>
                    </tr>
                    </table>
                    <table id="paid" >
                    <tr>
                            <th  style="text-align: left;">Name</th>
                            <th style="text-align: right;">Amount</th>
                    </tr>
                    <tr>
                        <td  style="text-align: left;">Tution Fee</td>
                        <td style="text-align: right;">{{$challan->original_amount}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Van Rent</td>
                    <td style="text-align: right;">{{$challan->transport_fee}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Discount Fee</td>
                    <td style="text-align: right;">{{$challan->discount_amount}}</td>
                    </tr>
                    <tr>
                        <td  style="text-align: left;">Miscellaneous</td>
                        <td style="text-align: right;">{{$challan->miscellaneous}}</td>
                    </tr>
                    <tr>

                    <td  style="text-align: left;">Grand Total</td>
                    <td style="text-align: right;">{{$challan->amount_paid}}</td>
                    </tr>
                    </table>
                    <span>For bank purpose only: Transfer to Accounts</span>
                    <table id="paid" >
                    <tr>
                            <td colspan="3" style="text-align: left;border:0px;padding-right: 50px;">Challan <span style="border: 1px solid #000;padding: 5px;margin-left: 100px;">{{$challan->challan_number}}</span></td>
                    </tr>
                    <tr>
                        <td  style="text-align: left;">Credit</td>
                        <td style="text-align: left;">A/C690363</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Payable within due date (Rs.)</td>
                    <td style="text-align: right;">{{$challan->amount_paid}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Payable after due date (RS.)</td>
                    <td style="text-align: right;"></td>
                    </tr>
                    </table>
                    <table id="paid" style="margin-top: 17px;">
                        <tr>
                            <th style="border: 0px ">Cash Officer</th>
                            <th style="border: 0px ;">Managar Opps.</th>
                        </tr>
                    </table>
                    <table>
                        <tr style="text-align: center;">
                            <td style="font-size: 8px;text-align: center;"><strong>Note:</strong>Rs.30/- Per day will be charged after due date.</td>
                        </tr>
                        <tr style="text-align: center;">
                            <td style="font-size: 8px;text-align: center;">Rs.30/- will be charged in case of Re-issuance</td>
                        </tr>
                    </table>
                </div>
                <div class="column">

                    <table id="customers">
                    <tr>
                            <th colspan="3" style="border:0px">The Grammar Higher Secondary School</th>
                    </tr>
                    <tr>
                        <th colspan="3"  style="border:0px;font-weight:normal;">Student Copy</th>
                    </tr>
                    <tr>
                        <td  style="width: 20%;border:0px;text-align:center;"><img style="width: 70%;" src="{{asset('JS.jpg')}}"></td>
                        <td style="font-size:14px;text-align:center;border:0px">DINGA (GUJRAT) <br>PH# 0537-670318 </td>
                        <td style="width: 20%;border:0px;text-align:center;"><img style="width: 70%;" src="{{asset('logo.jpg')}}" ></td>
                    </tr>
                    <tr>
                        <th colspan="3" style="border:0px;padding-bottom: 12px;"><span style="border: 1px solid #ddd;padding: 5px;"> Fee Challan </span></th>
                    </tr>
                    <tr>
                        <td>Challan #</td>
                        <td colspan="2">{{$challan->challan_number}}</td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td colspan="2">{{$challan->student_name}}</td>
                    </tr>
                    <tr>
                        <td>Father's Name</td>
                        <td colspan="2">{{$challan->father_name}}</td>
                    </tr>
                    <tr>
                        <td>CNIC No Of Father/Guardian:</td>
                        <td colspan="2">{{$challan->father_cnic}}</td>
                    </tr>
                    <tr>
                        <td>Class:</td>
                        <td colspan="2">{{$challan->class}}</td>
                    </tr>
                    <tr>
                        <td>Due Date:</td>
                        <td colspan="2">{{date('d-m-Y',strtotime($challan->last_date))}}</td>
                    </tr>
                    <tr>
                        <td>Period:</td>
                        <td colspan="2">{{date('d-m-Y',strtotime($challan->challan_month))}}</td>
                    </tr>
                    </table>
                    <table id="paid" >
                    <tr>
                            <th  style="text-align: left;">Name</th>
                            <th style="text-align: right;">Amount</th>
                    </tr>
                    <tr>
                        <td  style="text-align: left;">Tution Fee</td>
                        <td style="text-align: right;">{{$challan->original_amount}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Van Rent</td>
                    <td style="text-align: right;">{{$challan->transport_fee}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Discount Fee</td>
                    <td style="text-align: right;">{{$challan->discount_amount}}</td>
                    </tr>
                    <tr>
                        <td  style="text-align: left;">Miscellaneous</td>
                        <td style="text-align: right;">{{$challan->miscellaneous}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Grand Total</td>
                    <td style="text-align: right;">{{$challan->amount_paid}}</td>
                    </tr>
                    </table>
                    <span>For bank purpose only: Transfer to Accounts</span>
                    <table id="paid" >
                    <tr>
                            <td colspan="3" style="text-align: left;border:0px;padding-right: 50px;">Challan <span style="border: 1px solid #000;padding: 5px;margin-left: 100px;">{{$challan->challan_number}}</span></td>
                    </tr>
                    <tr>
                        <td  style="text-align: left;">Credit</td>
                        <td style="text-align: left;">A/C690363</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Payable within due date (Rs.)</td>
                    <td style="text-align: right;">{{$challan->amount_paid}}</td>
                    </tr>
                    <tr>
                    <td  style="text-align: left;">Payable after due date (RS.)</td>
                    <td style="text-align: right;"></td>
                    </tr>
                    </table>
                    <table id="paid" style="margin-top: 17px;">
                    <tr>
                        <th style="border: 0px ">Cash Officer</th>
                        <th style="border: 0px ;">Managar Opps.</th>
                    </tr>

                    </table>
                    <table>
                        <tr style="text-align: center;">
                            <td style="font-size: 8px;text-align: center;"><strong>Note:</strong>Rs.30/- Per day will be charged after due date.</td>
                        </tr>
                        <tr style="text-align: center;">
                            <td style="font-size: 8px;text-align: center;">Rs.30/- will be charged in case of Re-issuance</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    @if(!isset($footer_not) || $footer_not == 0)
</div>

</body>
</html>
@endif
