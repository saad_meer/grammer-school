
@extends('layouts.header')
@section('title','User Activity')
@section('styles')
<style>
    div.dataTables_wrapper {
        margin: 0 auto;
    }
</style>
@endsection
@section('datatables')
<link href="{{url('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">

@endsection
@section('content')


    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <h4 class="mg-b-0 tx-spacing--1">User Activity</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <table id="data_display" class="table display nowrap text-center">
                          <thead>
                            <tr>
                                <th>User</th>
                                <th>Url</th>
                                <th>Message</th>
                                <th>login Time</th>
                            </tr>
                          </thead>
                            <tbody>

                            </tbody>
                        </table>
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection

@section('scripts')
<script src="{{url('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>


<script>
    $(document).ready(function () {
        $('#data_display').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('admin/useractivitydisplay') }}",
                     "dataType": "json",
                     "type": "POST",
                   },
            "columns": [
                { "data": "user" },
                { "data": "url" },
                { "data": "message" },
                { "data": "login" }
            ]


        });
    });

</script>

@endsection
