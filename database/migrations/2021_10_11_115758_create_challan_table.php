<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChallanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challan', function (Blueprint $table) {
            $table->id();
            $table->string('challan_number')->nullable();
            $table->string('registration_number')->nullable();
            $table->string('student_name')->nullable();
            $table->string('father_name')->nullable();
            $table->string('father_cnic')->nullable();
            $table->string('class')->nullable();
            $table->Integer('student_id')->nullable();
            $table->date('challan_month')->nullable();
            $table->decimal('original_amount',10,2)->nullable();
            $table->decimal('transport_fee',10,2)->nullable();
            $table->Integer('fee_discount')->nullable();
            $table->decimal('discount_amount',10,2)->nullable();
            $table->decimal('late_fee_amount',10,2)->nullable();
            $table->decimal('amount_paid',10,2)->nullable();
            $table->date('challan_paid_date')->nullable();
            $table->enum('challan_status',['Paid','Unpaid'])->default('Unpaid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challan');
    }
}
