
@extends('layouts.header')
@section('title','Setting')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Setting</h4>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Add Setting" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/settings/addsettings')}}" method="POST">

                            <div class="form-row">
                                @if(isset($student_class))
                                    @foreach ($student_class as $key=>$student_record)
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">Class <span class="text-danger">*</span></label>
                                            <input type="text" disabled class="form-control" value="{{$student_record->student_class}}" name="student_class" placeholder="Student Class">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="inputEmail4">Fee <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" value="{{isset($student_record->class_fee)?$student_record->class_fee:''}}" name="student_fee[{{$student_record->student_class}}]" placeholder="Fee">
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">


$('#addform').submit(function(event) {
    var url = '{{url("/")}}';

//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addform')[0].reset();
            swalsuccess(data.message,url+'/admin/settings/add');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



