<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\FeeStructure;
use App\Models\Admin\Student;
use App\Models\Admin\Challan;
use App\Models\Admin\Download;
use App\Models\Admin\UserActivity;
use Validator;
use View;
use PDF;
use App\Jobs\MakeChallanStoreJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Queue;
class ChallanController extends Controller
{
    public function index()
    {
        return view('challan.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'challan_number',
            1 =>'student_name',
            2=> 'class',
            3=> 'challan_month',
            4=> 'transport_fee',
            5=> 'discount_amount',
            6=> 'late_fee_amount',
            7=> 'amount_paid',
            8=> 'id',
        );

            $totalData = Challan::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $results = Challan::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            }
            else {
            $search = $request->input('search.value');

            $results =  Challan::where('challan_number','LIKE',"%{$search}%")
                        ->orWhere('student_name', 'LIKE',"%{$search}%")
                        ->orWhere('father_name', 'LIKE',"%{$search}%")
                        ->orWhere('class', 'LIKE',"%{$search}%")
                        ->orWhere('challan_month', 'LIKE',"%{$search}%")
                        ->orWhere('transport_fee', 'LIKE',"%{$search}%")
                        ->orWhere('fee_discount', 'LIKE',"%{$search}%")
                        ->orWhere('discount_amount', 'LIKE',"%{$search}%")
                        ->orWhere('late_fee_amount', 'LIKE',"%{$search}%")
                        ->orWhere('amount_paid', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = Challan::where('challan_number','LIKE',"%{$search}%")
                                ->orWhere('student_name', 'LIKE',"%{$search}%")
                                ->orWhere('father_name', 'LIKE',"%{$search}%")
                                ->orWhere('class', 'LIKE',"%{$search}%")
                                ->orWhere('challan_month', 'LIKE',"%{$search}%")
                                ->orWhere('transport_fee', 'LIKE',"%{$search}%")
                                ->orWhere('fee_discount', 'LIKE',"%{$search}%")
                                ->orWhere('discount_amount', 'LIKE',"%{$search}%")
                                ->orWhere('late_fee_amount', 'LIKE',"%{$search}%")
                                ->orWhere('amount_paid', 'LIKE',"%{$search}%")
                                 ->count();
            }

            $data = array();
            if(!empty($results))
            {
                $delete='';
            foreach ($results as $value)
            {

            $edit =  url('admin/challan/view',$value->id );

            if($delete=='admin')
            {
                $delete='<a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>';
            }

            $nestedData['challan_number'] = $value->challan_number;
            $nestedData['student_name'] =$value->student_name;
            $nestedData['class'] =  $value->class;
            $nestedData['challan_month'] =  date('M',strtotime($value->challan_month));
            $nestedData['transport_fee'] =  $value->transport_fee;
            $nestedData['discount_amount'] =  $value->discount_amount;
            $nestedData['late_fee_amount'] =  $value->late_fee_amount;
            $nestedData['amount_paid'] =  $value->amount_paid;

            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="'.$edit.'" target="__blank"><i class="fas fa-eye"></i>  View</a>
                '.$delete.'
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $student_class=FeeStructure::all();
        return view('challan.add',compact('student_class'));
    }
    public function addProcess(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'student_class' =>    'required',
            'student'        =>   'required',
            'challan_month'         =>   'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {

            $get_fee=FeeStructure::where('student_class',$request->student_class)->first('class_fee');
            if($get_fee)
            {
                $get_student=Student::where('student_status','Active')->where('class',$request->student_class);
                if($request->student!='All')
                {
                    $get_student= $get_student->where('id',$request->student);
                }
                $get_student= $get_student->get();
                if(count($get_student)>0)
                {
                    $view = View::make('challan.genratedstudent', ['student' => $get_student,'get_fee'=> $get_fee,'month'=>$request->challan_month,'miscellaneous'=>$request->miscellaneous])->render();
                    return response()->json(['code'=>200,'html'=>$view]);
                }

            }
        }

    }
    public function update()
    {
        $student_class=FeeStructure::all();
        return view('challan.update',compact('student_class'));
    }
    public function updatechallan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_class' =>    'required',
            'student'        =>   'required',
            'challan_month'         =>   'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $challan=Challan::where('class',$request->student_class)
                    ->whereMonth('challan_month', date('m',strtotime($request->challan_month)))
                    ->whereYear('challan_month',date('Y',strtotime($request->challan_month)))
                    ->where('challan_status','Unpaid');

                if($request->student!='All')
                {
                    $challan= $challan->where('student_id',$request->student);
                }
            $challan=$challan->get();

                $view = View::make('challan.updatechallan', ['challan' => $challan])->render();

                return response()->json(['code'=>200,'html'=>$view]);

        }
    }
    public function delete($id)
    {
        $delete=Challan::where('id','=',$id)->first();
        if($delete)
        {

            if( $delete->challan_status=='Paid')
            {
                return response()->json(['code'=>404,'message'=>'Paid Challan Cannot be deleted']);
            }
            $delete->delete();
            return response()->json(['code'=>200,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
    public function show($id)
    {
        if($id>0)
        {
            $challan=Challan::whereId($id)->first();
            return view('challan.pdf.challan',compact('challan'));
        }
    }
    public function getstudent(Request $request)
    {
        if(isset($request->student_class))
        {
            $result=Student::where('class',$request->student_class)->get();
            if(count($result)>0)
            {
                return response()->json(['code'=>200,'result'=> $result]);
            }
            else
            {
                return response()->json(['code'=>404,'result'=> '']);

            }
        }
    }
    public function genratechallan(Request $request)
    {
        $get_fee=FeeStructure::where('student_class',$request->std_class)->first('class_fee');
            if($get_fee)
            {
                $input=$request->all();
                $html='';
                $get_student=Student::where('student_status','Active')->where('class',$request->std_class);
                if($request->student!='All')
                {
                    $get_student= $get_student->where('id',$request->student);
                }
                $month=date('M',strtotime($request->challan_month));
                $get_student= $get_student->get();
                $challancount=count($get_student);

                if(count($get_student)>1)
                {
                    MakeChallanStoreJob::dispatch($get_student,$input);
                }
                else if (count($get_student)==1)
                {

                    foreach ($get_student as $stdkey=>$data)
                    {
                        $get_challan=Challan::where('student_id',$request->student)->whereMonth('challan_month', date('m',strtotime($request->challan_month)))
                        ->whereYear('challan_month',date('Y',strtotime($request->challan_month)))->first();
                        if(!$get_challan)
                        {
                            $total_fee=$get_fee->class_fee+$data->transport_fee+$request->miscellaneous;
                            $discounted_fee=($total_fee*$data->fee_discount)/100;
                            $total_fee=$total_fee- $discounted_fee;

                            $challan=new Challan();
                            $challan->challan_number=$data->class.'-'.random_int(100000, 999999);
                            $challan->registration_number=$data->registration_number;
                            $challan->student_name=$data->student_name;
                            $challan->father_name=$data->father_name;
                            $challan->father_cnic=$data->father_cnic;
                            $challan->class=$data->class;
                            $challan->original_amount=$get_fee->class_fee;
                            $challan->transport_fee=$data->transport_fee;
                            $challan->fee_discount=$data->fee_discount;
                            $challan->student_id=$data->id;
                            $challan->challan_month=db_format_date($request->challan_month);
                            $challan->last_date=db_format_date($request->last_date);
                            $challan->discount_amount= $discounted_fee;
                            $challan->amount_paid=$total_fee;
                            $challan->miscellaneous=$request['miscellaneous'];
                            $challan->challan_status='Unpaid';

                            $challan->save();

                            if($stdkey==0)
                            {
                                $view = view('challan.pdf.challan', array('header_not' => 0, 'footer_not' => 1, 'challan' => $challan));
                            }
                            $html .= $view->render();
                            $pdf = PDF::loadHTML($html);
                            $sheet = $pdf->setPaper('a4', 'landscape');
                            $challan_name=$request->std_class.'-'.date('F Y',strtotime($request->challan_month)).'-'.time();
                            $challan_path='pdf/'.$request['std_class'].'-'.date('F Y',strtotime($request->challan_month)).'-'.time().'.pdf';
                            $path = public_path('pdf').'/'.$request->std_class.'-'.date('F Y',strtotime($request->challan_month)).'-'.time().'.pdf';
                            $pdf->save($path);

                            $user='';
                            if($request->student!='All')
                            {
                                $user=Student::where('id',$request->student)->first('student_name');
                                $user=$user->student_name;
                            }
                            $download=new Download();
                            $download->name=$challan_name;
                            $download->path=$challan_path;
                            $download->challan_created_of=$user;
                            $download->save();
                        }
                    }
                }
                $user_data=[
                    'user_id'=>auth()->user()->id,
                    'url'=>'',
                    'message'=> 'created Challan of Class '. $request->std_class,
                    'record_id'=>''
                ];
                $user_activity=new UserActivity();
                $user_activity->saverecord($user_data);
                return response()->json(['code'=>200,'message'=>'Challan Genrated successfully']);

            }
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
    }
    public function updatechallanProcess(Request $request)
    {
        $input=$request->data;
        if(!empty($input) && count($input)>0)
        {
            foreach($input as $key=>$value)
            {
                $get_challan=Challan::where('challan_number',$key)->first();
                if($get_challan)
                {
                    $challan_amount=$get_challan->amount_paid+$value['amount'];
                    $get_challan->late_fee_amount=isset($value['amount'])?$value['amount']:0;
                    $get_challan->amount_paid= $challan_amount;
                    $get_challan->challan_status=$value['status'];
                    $get_challan->challan_paid_date=date('Y-m-d');
                    $get_challan->save();
                }
            }
            $user_data=[
                'user_id'=>auth()->user()->id,
                'url'=>'',
                'message'=> 'updated Challan',
                'record_id'=>''
            ];
            $user_activity=new UserActivity();
            $user_activity->saverecord($user_data);

        }
        return redirect('admin/challan/display')->with('message', 'Challan updated successfully');
    }
    public function challandownloaddisplay(Request $request)
    {
        return view('challan.download');
    }
    public function challandownload(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'name',
            2 =>'challan_created_of',
            3=> 'path',
            4=> 'created_at',
            5=> 'id',
        );

            $totalData = Download::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $results = Download::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            }
            else {
            $search = $request->input('search.value');

            $results =  Download::where('name','LIKE',"%{$search}%")
                        ->orWhere('path', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = Download::where('name','LIKE',"%{$search}%")
                                 ->orWhere('path', 'LIKE',"%{$search}%")
                                 ->count();
            }

            $data = array();
            if(!empty($results))
            {

            foreach ($results as $value)
            {

            $nestedData['name'] = $value->name;
            $nestedData['created'] = $value->challan_created_of;
            if($value->challan_genrated==1)
            {
                $nestedData['path'] ='<a href="'.asset($value->path).'" target="__blank">Click to view</a>';
            }
            else
            {
                $nestedData['path'] ='<a href="'.url('admin/genrate_challan').'/'.$value->id.'">Download</a>';
            }

            $nestedData['created_at'] =  format_date_time($value->created_at);
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
}
