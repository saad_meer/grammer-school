<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    public $table = 'user_activity';

    public function saverecord($data)
    {
        $user_activity=new UserActivity();
        $user_activity->user_id=$data['user_id'];
        $user_activity->url=$data['url'];
        $user_activity->message=$data['message'];
        $user_activity->record_id=$data['record_id'];
        $user_activity->save();

        return true;
    }

}
