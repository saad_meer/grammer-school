@extends('web.layouts.header')
@section('content')

  <div class="container main-container container-xxl my-5">
    <div class="row gy-5">
      <div class="col-lg-8 col-xxl-9">
         <!-- ========================= Quotes-card section ========================== -->
        <div class="row gy-4 quotes-card" data-masonry='{"percentPosition": true }'>

          @if(isset($result['category']) && count($result['category'])>0)
          <div class="col-sm-6">
            <h2 class="card-title right-side-card-heading ">
                <i class="fas fa-list-alt text-primary"></i> &nbsp;
                <span class="m-auto">Category</span></h2>
            <ul class="list-group">
                @foreach ($result['category'] as $key=>$value)
                    <li class="list-group-item"><a style="color:inherit;" href="{{url('categories/'.$value->category_slug)}}">{{$value->category_name}}</a></li>
                @endforeach
            </ul>
          </div>

          @endif
          @if(isset($result['author']) && count($result['author'])>0)
          <div class="col-sm-6">
            <h2 class="card-title right-side-card-heading ">
                <i class="fas fa-list-alt text-primary"></i> &nbsp;
                <span class="m-auto">Authors</span></h2>
            <ul class="list-group">
                @foreach ($result['author'] as $key=>$value)
                    <li class="list-group-item"><a style="color:inherit;" href="{{url('authors/'.$value->author_first_name.'-'.$value->author_last_name)}}">{{$value->author_first_name.' '.$value->author_last_name}}</a></li>
                @endforeach
            </ul>
          </div>

          @endif
        </div>
      </div>
       <!-- ========================= Right-Side-Section ========================== -->
      <div class="col-lg-4 col-xxl-3 ps-xl-5  d-none d-lg-block">
        <div class="card ">
          <div class="card-body">
             <div class="d-flex">
              <h2 class="card-title right-side-card-heading ">
                <i class="fas fa-birthday-cake text-primary"></i> &nbsp;
                <span class="m-auto">Today's Birthdays</span></h2>

             </div>
              <hr>
              <!--Accordion wrapper-->
              <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                @include('web.birthday')
                <!-- Accordion card -->

              </div>
              <!-- Accordion wrapper -->
          </div>
        </div>

      </div>
    </div>
  </div>
@endsection





