
@extends('layouts.header')
@section('title','Dashboard')
@section('content')
    <div class="content  pd-0">


      <div class="content-body">
        <div class="container pd-x-0">
          <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                </ol>
              </nav>
              <h4 class="mg-b-0 tx-spacing--1">Welcome to  Grammer Higher Secondary School</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-12 mg-t-10">
                <div class="card">
                  <div class="card-header">
                    <h6 class="mg-b-0">Students Stats</h6>
                  </div><!-- card-header -->
                  <div class="card-body pd-0">
                    <div class="row no-gutters">
                      <div class="col col-sm-6 col-lg">
                        <div class="crypto">
                          <div class="media mg-b-10">
                            <div class="media-body pd-l-8">
                              <h6 class="tx-11 tx-spacing-1 tx-uppercase tx-semibold mg-b-5">Total Student</h6>
                              <div class="d-flex align-items-baseline tx-rubik">
                                <h5 class="tx-20 mg-b-0">{{$total_student}}</h5>
                              </div>
                            </div><!-- media-body -->
                          </div><!-- media -->
                        </div><!-- crypto -->
                      </div>
                      <div class="col col-sm-6 col-lg bd-t bd-sm-t-0 bd-sm-l">
                        <div class="crypto">
                          <div class="media mg-b-10">
                            <div class="media-body pd-l-8">
                              <h6 class="tx-11 tx-spacing-1 tx-uppercase tx-semibold mg-b-5">Active Students</h6>
                              <div class="d-flex align-items-baseline tx-rubik">
                                <h5 class="tx-20 mg-b-0">{{$active_student}}</h5>
                              </div>
                            </div><!-- media-body -->
                          </div><!-- media -->
                        </div><!-- crypto -->
                      </div>
                      <div class="col col-sm-6 col-lg bd-t bd-lg-t-0 bd-lg-l">
                        <div class="crypto">
                          <div class="media mg-b-10">
                            <div class="media-body pd-l-8">
                              <h6 class="tx-11 tx-spacing-1 tx-uppercase tx-semibold mg-b-5">InActive Students</h6>
                              <div class="d-flex align-items-baseline tx-rubik">
                                <h5 class="tx-20 mg-b-0">{{$inactive_student}}</h5>
                              </div>
                            </div><!-- media-body -->
                          </div><!-- media -->
                        </div><!-- crypto -->
                      </div>
                    </div><!-- row -->
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
          </div>
          <div class="row">
            <div class="col-12 mg-t-10">
                <div class="card">
                  <div class="card-header">
                    <h6 class="mg-b-0">Challan Stats ({{date('F')}})</h6>
                  </div><!-- card-header -->
                  <div class="card-body pd-0">
                    <div class="row no-gutters">
                      <div class="col col-sm-6 col-lg">
                        <div class="crypto">
                          <div class="media mg-b-10">
                            <div class="media-body pd-l-8">
                              <h6 class="tx-11 tx-spacing-1 tx-uppercase tx-semibold mg-b-5">Total Challan</h6>
                              <div class="d-flex align-items-baseline tx-rubik">
                                <h5 class="tx-20 mg-b-0">{{$total_challan}}</h5>
                              </div>
                            </div><!-- media-body -->
                          </div><!-- media -->
                        </div><!-- crypto -->
                      </div>
                      <div class="col col-sm-6 col-lg bd-t bd-sm-t-0 bd-sm-l">
                        <div class="crypto">
                          <div class="media mg-b-10">
                            <div class="media-body pd-l-8">
                              <h6 class="tx-11 tx-spacing-1 tx-uppercase tx-semibold mg-b-5">Paid Challan</h6>
                              <div class="d-flex align-items-baseline tx-rubik">
                                <h5 class="tx-20 mg-b-0">{{$paid_challan}}</h5>
                              </div>
                            </div><!-- media-body -->
                          </div><!-- media -->
                        </div><!-- crypto -->
                      </div>
                      <div class="col col-sm-6 col-lg bd-t bd-lg-t-0 bd-lg-l">
                        <div class="crypto">
                          <div class="media mg-b-10">
                            <div class="media-body pd-l-8">
                              <h6 class="tx-11 tx-spacing-1 tx-uppercase tx-semibold mg-b-5">UnPaid Challan</h6>
                              <div class="d-flex align-items-baseline tx-rubik">
                                <h5 class="tx-20 mg-b-0">{{$unpaid_challan}}</h5>
                              </div>
                            </div><!-- media-body -->
                          </div><!-- media -->
                        </div><!-- crypto -->
                      </div>
                    </div><!-- row -->
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
          </div>
          <div class="row">
            <div class="col-sm-6 col-lg-4 mg-t-10">
                <div class="card ht-md-100p">
                  <div class="card-header d-flex justify-content-between">
                    <h6 class="lh-5 mg-b-0">Challan Paid History</h6>
                    <a href="{{url('admin/challan/display')}}" target="__blank" class="tx-12 link-03">View All Challan <i data-feather="arrow-right" class="wd-15 ht-15"></i></a>
                  </div><!-- card-header -->
                  <div class="card-body pd-0">
                    <ul class="list-unstyled mg-b-0">
                        @if(isset($challan_list))
                            @foreach ($challan_list as $data)
                                <li class="list-item">
                                    <div class="media align-items-center">
                                    <div class="wd-35 ht-35 bd bd-2 bd-info tx-info rounded-circle align-items-center justify-content-center op-6 d-none d-sm-flex">
                                        <i data-feather="download" class="wd-20 ht-20"></i>
                                    </div>
                                    <div class="media-body mg-sm-l-15">
                                        <p class="tx-medium mg-b-0">{{$data->student_name}}</p>
                                        <p class="tx-12 mg-b-0 tx-color-03">Received {{$data->amount_paid}}</p>
                                    </div><!-- media-body -->
                                    </div><!-- media -->
                                    <div class="text-right tx-rubik">
                                    <p class="mg-b-0">Rs. {{$data->amount_paid}}</p>
                                    </div><!-- media-body -->
                                </li>
                            @endforeach
                        @endif

                    </ul>
                  </div><!-- card-body -->
                </div><!-- card -->
              </div><!-- col -->
          </div>
        </div><!-- container -->
      </div>
    </div>
    @endsection
    @section('scripts')


    @endsection

