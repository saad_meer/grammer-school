<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Student;
use App\Models\Admin\FeeStructure;
use App\Models\Admin\UserActivity;
use Validator;
use File;
use App\Imports\StudentImport;
use Maatwebsite\Excel\Facades\Excel;
class StudentsController extends Controller
{
    public function index()
    {
        return view('student.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'registration_number',
            1 =>'student_name',
            2=> 'father_name',
            3=> 'class',
            4=> 'student_status',
            5=> 'id',
        );

            $totalData = Student::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $results = Student::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            }
            else {
            $search = $request->input('search.value');

            $results =  Student::where('registration_number','LIKE',"%{$search}%")
                        ->orWhere('student_name', 'LIKE',"%{$search}%")
                        ->orWhere('father_name', 'LIKE',"%{$search}%")
                        ->orWhere('class', 'LIKE',"%{$search}%")
                        ->orWhere('student_status', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered =  Student::where('registration_number','LIKE',"%{$search}%")
                                ->orWhere('student_name', 'LIKE',"%{$search}%")
                                ->orWhere('father_name', 'LIKE',"%{$search}%")
                                ->orWhere('class', 'LIKE',"%{$search}%")
                                ->orWhere('student_status', 'LIKE',"%{$search}%")
                                 ->count();
            }

            $data = array();
            if(!empty($results))
            {
                $delete='';

            foreach ($results as $value)
            {
            $edit =  url('/admin/students/edit',$value->id);
            if(auth()->user()->role =='admin')
            {
                $delete='<a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>';
            }


            $nestedData['reg_number'] = $value->registration_number;
            $nestedData['student'] =$value->student_name;
            $nestedData['father'] = $value->father_name;
            $nestedData['class'] =  $value->class;
            $nestedData['student_status'] =  $value->student_status;
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              '.$delete.'
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $student_class=FeeStructure::all();
        return view('student.add',compact('student_class'));
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'registration_number' =>    'required|unique:students,registration_number',
            'student_name'        =>   'required',
            'father_name'         =>   'required',
            'father_cnic'         =>   'required',
            'class'               =>   'required',
            'dob'               =>   'required',
        ],
        [
            'registration_number.required' => 'Registration Number is Required',
            'student_name.required' => 'Student Name is Required',
            'father_name.required' => 'Father is Required',
            'father_cnic.required' => 'Father Cnic is Required|min:6',
            'class.required' => 'Class is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $data= new Student();
            $data->registration_number=strtoupper($request->registration_number);
            $data->student_name=ucfirst($request->student_name);
            $data->father_name=ucfirst($request->father_name);
            $data->father_cnic= $request->father_cnic;
            $data->class=$request->class;
            $data->dob=db_format_date($request->dob);
            $data->transport_fee=$request->transport_fee;
            $data->fee_discount=$request->fee_discount;
            $data->student_status=$request->student_status;
            $data->gender=$request->gender;
            $data->save();

            $user_data=[
                'user_id'=>auth()->user()->id,
                'url'=>'admin/students/edit/'.$data->id,
                'message'=> 'created '. $request->student_name.' student',
                'record_id'=>$data->id
            ];
            $user_activity=new UserActivity();
            $user_activity->saverecord($user_data);


            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $student=Student::where('id','=',$id)->first();
        $student_class=FeeStructure::all();
        if($student)
        {
            return view('student.edit',compact('student','student_class'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $data=Student::where('id','=',$request->id)->first();
        if($data)
        {
            $validator = Validator::make($request->all(), [
                'registration_number' =>   'unique:students,registration_number,'.$request->id,
                'student_name'        =>   'required',
                'father_name'         =>   'required',
                'father_cnic'         =>   'required',
                'class'               =>   'required',
                'dob'                 =>   'required',
            ],
            [
                'student_name.required' => 'Student Name is Required',
                'father_name.required' => 'Father is Required',
                'father_cnic.required' => 'Father Cnic is Required|min:6',
                'class.required' => 'Class is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $data->student_name=ucfirst($request->student_name);
                $data->father_name=ucfirst($request->father_name);
                $data->father_cnic= $request->father_cnic;
                $data->class=$request->class;
                $data->dob=db_format_date($request->dob);
                $data->transport_fee=$request->transport_fee;
                $data->fee_discount=$request->fee_discount;
                $data->student_status=$request->student_status;
                $data->gender=$request->gender;
                $data->save();

                $user_data=[
                    'user_id'=>auth()->user()->id,
                    'url'=>'admin/students/edit/'.$request->id,
                    'message'=> 'edit record '. $request->student_name.' student',
                    'record_id'=>$request->id
                ];
                $user_activity=new UserActivity();
                $user_activity->saverecord($user_data);

                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('admin/sub-category/display');

    }
    public function delete($id)
    {
        $delete=Student::where('id','=',$id)->first();
        if($delete)
        {
            $delete->delete();
            $user_data=[
                'user_id'=>auth()->user()->id,
                'url'=>'',
                'message'=> 'delete student record',
                'record_id'=>$id
            ];
            $user_activity=new UserActivity();
            $user_activity->saverecord($user_data);

            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
    public function useractivity()
    {
        return view('activity.index');
    }
    public function useractivitydisplay(Request $request)
    {
        $columns = array(
            0 =>'user_id',
            1=> 'url',
            2=> 'message',
            3=> 'login_time',
            4=> 'id'
        );

            $totalData = UserActivity::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $results = UserActivity::select('users.first_name','users.last_name','user_activity.*')
                        ->leftjoin('users','users.id','user_activity.user_id')->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            }
            else {
            $search = $request->input('search.value');

            $results =  UserActivity::select('users.first_name','users.last_name','user_activity.*')
                        ->leftjoin('users','users.id','user_activity.user_id')
                        ->where('users.first_name','LIKE',"%{$search}%")
                        ->orWhere('users.last_name', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = UserActivity::select('users.first_name','users.last_name','user_activity.*')
                                ->leftjoin('users','users.id','user_activity.user_id')
                                ->where('users.first_name','LIKE',"%{$search}%")
                                ->orWhere('users.last_name', 'LIKE',"%{$search}%")
                                 ->count();
            }

            $data = array();
            if(!empty($results))
            {

                foreach ($results as $value)
                {

                    $nestedData['user'] = $value->first_name.' '.$value->last_name;
                    $nestedData['url'] =  '<a href='.url($value->url).'>view</a>';
                    $nestedData['message'] = $value->first_name.' '.$value->last_name.' '.$value->message;
                    $nestedData['login'] =  isset($value->login_time)?date('d/m/Y h:m:s',strtotime($value->login_time)):'';
                    $data[] = $nestedData;

                }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function upgrade()
    {
        $student_class=FeeStructure::all();
        return view('student.upgrade',compact('student_class'));
    }
    public function promote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'student_class' => 'required',
            'student_upgrade_class' => 'required',
        ],
        [
            'student_class.required' => 'Intial Class is required is Required',
            'student_upgrade_class.required' => 'Upgrade Class is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $student=$request->student;
            if(!empty($student) && !in_array('All',$student))
            {
                foreach($student as $value)
                {
                    Student::where('id',$value)->update(['class'=>$request->student_upgrade_class]);
                }
            }
            else
            {
                Student::where('class',$request->student_class)->update(['class'=>$request->student_upgrade_class]);
            }

            return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);
        }
    }
    public function import_student()
    {
       dd('done');

        ini_set('memory_limit', -1);
        $file = public_path('csvs/Prep.csv');
        $studentArr = $this->csvToArray($file);
     //   dd($studentArr);

        foreach($studentArr as $key => $single){
            if(!empty($single['D.O.B']))
            {
                $single['D.O.B']=db_format_date($single['D.O.B']);
            }
            else
            {
                $single['D.O.B']='';
            }
            $data= new Student();
            $data->registration_number=$single['Reg #'];
            $data->student_name=$single['ST_Name'];
            $data->father_name=$single['F/Name'];
            $data->father_cnic= $single['CNIC'];
            $data->class='Prep';
            $data->dob=$single['D.O.B'];
            $data->transport_fee=$single['Rent'];
            $data->student_status='Active';
            $data->save();
        }
        dd('done');
    }
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = array();
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            $count = 0;
            while (($row = fgetcsv($handle, 5000, $delimiter)) !== false)
            {
                if(count($row) < count($header)){
                    $def = (count($header)-count($row));
                    for($i=0; $i <= $def-1; $i++){
                        $row[] = "";
                    }
                }
                if (!$header) {
                    $header = $row;
                }else{

                    //dd($header, $row);
                    $data[] = array_combine($header, $row);
                }
                $count++;
            }
            fclose($handle);
        }

        return $data;
    }
}
