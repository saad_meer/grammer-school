@php
$background_image='';
$template_text_color='';
$template_text_style='';
$template_text_size='';
if(!empty($category->category_image))
{
    $background_image='background-image:url("storage/'.$category->category_image.'");';
}
else
{
    $background_image='background-color:#000';
}
@endphp
<div class="col-xxl-3  col-md-4 col-sm-6 py-">
<div class="card mh-100  text-center p-1" >
<div class="card-body d-flex justify-content-center align-items-center flex-column"  style="{{$background_image.$template_text_color.$template_text_style.$template_text_size}}">
    <h3 class="h3-responsive text-white quotes-heading">{!!$category->category_name!!}</h3>
</div>
<div class="card-footer bg-white py-1">
    <div class="d-flex  justify-content-around align-items-center">
    <div class="like">
        <p class="m-0 grey-text"><i class="fas fa-thumbs-up"></i> 100</p>
    </div>
    <div class="like">
        <p class="m-0 grey-text"><i class="fas fa-share"></i> 100</p>
    </div>
    </div>
</div>
</div>
</div>
