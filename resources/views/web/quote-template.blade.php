@php
$background_image='';
$template_text_color='';
$template_text_style='';
$template_text_size='';
$background_repeat='';
$background_size='';
if($quote->quote_has_image==1)
{
    $path_image=asset('storage/'.$quote->quote_image.'');
    $background_image='background-image:url("'.$path_image.'");';
    $background_repeat='background-repeat:no-repeat;';
    $background_size='background-size:cover;';

}
elseif($quote->template_id>0 && $quote->template_id!=null)
{
    if($quote->template_has_image==1 && $quote->template_has_image!='')
    {
        $path_image=asset('storage/'.$quote->template_image.'');
        $background_image='background-image:url("'.$path_image.'");';
    }
    else
    {
        $background_image='background-color:'.$quote->template_background_color.';';
    }
    if(!empty($quote->template_text_color))
    {
        $template_text_color='color:'.$quote->template_text_color.';';
    }
    if(!empty($quote->template_text_style))
    {
        $template_text_style='font-style:'.$quote->template_text_style.';';
    }
    if(!empty($quote->template_text_size))
    {
        $template_text_size='font-size:'.$quote->template_text_size.'px';
    }
}
@endphp
<div @if(isset($column_check)) class="col-lg-10 mx-auto" @else  class="col-xxl-3  col-md-4 col-sm-6 py-" @endif>
    <div class="card mh-100  text-center p-1" style="cursor: pointer;" onclick="detailmodel({{$quote->quotes_id}})">
        <div class="card-body d-flex justify-content-center align-items-center flex-column"  style="{{$background_size.$background_repeat.$background_image.$template_text_color.$template_text_style.$template_text_size}}">
            {{-- <h3 class="h3-responsive text-white quotes-heading">Smile</h3> --}}
            <p class="lead white-text quotes-text">{!!$quote->quote_text!!}</p>
        </div>
        @if(!isset($column_check))
        <div class="card-footer bg-white py-1 " >
            <div class="d-flex  justify-content-around  ">
                <div class="like">
                    <div class="fb-like" data-href="{{url('quotes/'.$quote->quotes_id)}}" data-width="12px" data-layout="button" data-action="like" data-size="small" data-share="true"></div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
