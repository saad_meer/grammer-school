
@extends('layouts.header')
@section('title','Challan')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Export Challan List</h4>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                    @endif
                    <div data-label="Genrate Student List" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/reports/challanlistexport')}}" method="GET">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Please Select Class</label>
                                        <select class="custom-select student_class" name="student_class">
                                            <option value="">Please Select Class</option>
                                            @foreach ($student_class as $student)
                                                <option value="{{$student->student_class}}">{{$student->student_class}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Please Select Student</label>
                                        <select class="custom-select" name="student" id="student">
                                            <option value="">Please Select Student</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Please Select Status</label>
                                        <select class="custom-select" name="challan_status">
                                            <option value="">Please Select Status</option>
                                            <option value="Paid">Paid</option>
                                            <option value="Unpaid">UnPaid</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">From <span class="text-danger">*</span> </label>
                                    <input type="text" class="form-control challan_month" placeholder="Date From" name="date_from" autocomplete="off">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">To <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control challan_month" placeholder="Date To" name="date_to" autocomplete="off">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">
 $(function(){
        'use strict'
        $('.challan_month').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });

    });
$(".student_class").change(function()
{
    var html='';
    var url = '{{url("/")}}';
    var student_class = $('.student_class').val();
    $.ajax({
        url: url+'/admin/get_student_class',
        type: 'POST',
        data: {
            student_class: student_class
        },
        success: function (data) {
            if(data.code==200)
            {
                html+='<option value="All">All Students</option>';
                $.each( data.result, function( key, value ) {
                    html+='<option value="'+value.id+'">'+value.student_name+'</option>';
                });
                console.log(html);
                $('#student').html(html);
            }
            if(data.code==404)
            {
                html='<option value="">Please Select Student</option>';
                $('#student').html(html);
            }
        },
        error: function (error) {
        // swalerror("SomeThing Went Wrong");
        }
    });
});

</script>
@endsection



