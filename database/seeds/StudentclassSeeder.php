<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

Class StudentclassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \DB::table('fee_structure')->delete();

        \DB::table('fee_structure')->insert(array (
            0 =>
            array (
                'id' => 1,
                'student_class' => 'Play',
            ),
            1 =>
            array (
                'id' => 2,
                'student_class' => 'Nursery',
            ),
            2 =>
            array (
                'id' => 3,
                'student_class' => 'Prep',
            ),
            3 =>
            array (
                'id' => 4,
                'student_class' => 'One',
            ),
            4 =>
            array (
                'id' => 5,
                'student_class' => 'Two',
            ),
            5 =>
            array (
                'id' => 6,
                'student_class' => 'Three',
            ),
            6 =>
            array (
                'id' => 7,
                'student_class' => 'Four',
            ),
            7 =>
            array (
                'id' => 8,
                'student_class' => 'Five',
            ),
            8 =>
            array (
                'id' => 9,
                'student_class' => 'Six',
            ),
            9 =>
            array (
                'id' => 10,
                'student_class' => 'Seven',
            ),
            10 =>
            array (
                'id' => 11,
                'student_class' => 'Eight',
            ),
            11 =>
            array (
                'id' => 12,
                'student_class' => 'Pre-Nine',
            ),
            12 =>
            array (
                'id' => 13,
                'student_class' => 'Nine',
            ),
            13 =>
            array (
                'id' => 14,
                'student_class' => 'Ten',
            ),
        ));
        \DB::table('users')->delete();

        DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'meer',
            'email'=>'admin@admin.com',
            'phone' => '+923004801494',
            'password' => Hash::make('admin123'),
            'role' => 'admin',
        ]);
    }
}
