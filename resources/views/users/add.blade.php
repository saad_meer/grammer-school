
@extends('layouts.header')
@section('title','Add Users')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Users</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/users/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Add Users" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/users/addProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Admin Type</label>
                                        <select class="custom-select" name="role_id">
                                            <option value="1" >Admin</option>
                                            <option value="2" >Customer</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">First Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Phone</label>
                                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Address</label>
                                    <input type="text" class="form-control" name="address" placeholder="Address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Role</label>
                                        <select class="custom-select" name="role">
                                            <option value="admin">Admin</option>
                                            <option value="employee">Employee</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="user_image" id="customFile">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">


$('#addform').submit(function(event) {
    var url = '{{url("/")}}';

//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addform')[0].reset();
            swalsuccess(data.message,url+'/admin/users/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



