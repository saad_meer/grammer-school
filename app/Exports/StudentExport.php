<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Admin\Student;


class StudentExport  implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($student_class,$status)
    {
        $this->student_class = $student_class;
        $this->status = $status;
    }

    public function view(): View
    {

        $get_student=Student::where('class',$this->student_class);
        if(!empty($this->status))
        {
            $get_student=$get_student->where('student_status',$this->status);
        }
        $get_student=$get_student->get();
        return view('exports.student',['student'=>$get_student]);
    }

}
