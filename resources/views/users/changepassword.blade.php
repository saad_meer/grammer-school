
@extends('layouts.header')
@section('title','Edit Profile')
@section('content')


    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Profile</h4>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Profile" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/users/editprofileProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Old Password<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="current_password" placeholder="Old Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">New Password<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="new_password" placeholder="New Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Confirm Password<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="new_confirm_password" placeholder="Confirm Password">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">

$('#addform').submit(function(event) {

//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addform')[0].reset();
            swalsuccess(data.message);
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



