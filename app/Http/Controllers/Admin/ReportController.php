<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\FeeStructure;
use App\Models\Admin\Challan;
use App\Models\Admin\Student;
use App\Exports\StudentExport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Carbon\Carbon;
use PDF;
use App\Models\Admin\Download;
use View;

class ReportController extends Controller
{
    public function studentlist()
    {
        $student_class=FeeStructure::all();
        return view('reports.studentlist',compact('student_class'));
    }
    public function getlist(Request $request)
    {
       if(!empty($request->student_class))
       {
            $get_student=Student::where('class',$request->student_class);
            if(!empty($request->status))
            {
                $get_student=$get_student->where('student_status',$request->status);
            }
            $get_student=$get_student->get();

            $html='';
            foreach($get_student as $key=>$value)
            {
                $html.='<tr><td>'.$value->registration_number.'</td>';
                $html.='<td>'.$value->student_name.'</td>';
                $html.='<td>'.$value->father_name.'</td>';
                $html.='<td>'.$value->class.'</td>';
                $html.='<td>'.$value->gender.'</td>';
                $html.='<td>'.$value->student_status.'</td></tr>';

            }
            return response()->json(['code'=>200,'html'=>$html]);
       }
    }
    public function studentexcelexport(Request $request)
    {

        $student_class=$request->std_class;
        $status=$request->status;
        return Excel::download(new StudentExport($student_class,$status), $student_class.'List.xlsx');
    }
    public function challanlist()
    {
        $student_class=FeeStructure::all();
        return view('reports.challanlist',compact('student_class'));
    }
    public function challanlistexport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_from' =>    'required',
            'date_to'        =>   'required',
        ]);
        if ($validator->fails()) {
            return redirect('admin/reports/challanlist')->with('message', $validator->errors()->first());
        }



        $startDate = db_format_date($request->date_from);

        $endDate = db_format_date($request->date_to);

        $challan=Challan::whereDate('challan_month', '>=', $startDate)
                        ->whereDate('challan_month', '<=', $endDate);

        if(isset($request->student_class) && !empty($request->student_class))
        {
            $challan= $challan->where('class',$request->student_class);
        }
        if(isset($request->student) && !empty($request->student) && ($request->student!='All'))
        {
            $challan= $challan->where('student_id',$request->student);
        }
        if(isset($request->challan_status) && !empty($request->challan_status))
        {
            $challan= $challan->where('challan_status',$request->challan_status);
        }
        $challan= $challan->get();
        $challan =$challan->sortBy('challan_month');
        $sort_array=array();
        if(isset($challan) && count($challan)>0)
        {
            foreach($challan as $value)
            {

                $sort_array[$value->challan_month][]=$value;
            }
        }
        $pdf = PDF::loadView('challan.pdf.genratechallanpaidlist',  array('challan' => $sort_array));


        return $pdf->download(time().'.pdf');


    }
    public function genratereport($id)
    {
        if($id>0)
        {
            $download=Download::where('id',$id)->first();
            $get_challan=Challan::where('class', $download->student_class)
                    ->whereMonth('challan_month', date('m',strtotime($download->downloads_challan_month)))
                    ->whereYear('challan_month',date('Y',strtotime($download->downloads_challan_month)))->get();
            $html='';
            $challancount=count($get_challan);
            if(count($get_challan)>0)
            {

                foreach ($get_challan as $stdkey=>$challan)
                {
                    if($stdkey==0)
                    {
                        $view = view('challan.pdf.challan', array('header_not' => 0, 'footer_not' => 1, 'challan' => $challan));
                    }
                    elseif($stdkey == ($challancount-1))
                    {
                        $view = view('challan.pdf.challan', array('header_not' => 1, 'footer_not' => 1,  'challan' => $challan));
                    }
                    else
                    {
                        $view = view('challan.pdf.challan', array('header_not' => 1, 'footer_not' => 1, 'challan' => $challan));
                    }
                    $html .= $view->render();
                }
            }

            $pdf = PDF::loadHTML($html);
            $sheet = $pdf->setPaper('a4', 'landscape');
            $challan_name=$download->student_class.'-'.date('F Y',strtotime($download->downloads_challan_month)).'-'.time();
            $challan_path='pdf/'.$download->student_class.'-'.date('F Y',strtotime($download->downloads_challan_month)).'-'.time().'.pdf';
            $path = public_path('pdf').'/'.$download->student_class.'-'.date('F Y',strtotime($download->downloads_challan_month)).'-'.time().'.pdf';
            $pdf->save($path);

            $download->path=$challan_path;
            $download->challan_genrated=1;
            $download->save();
            return redirect('admin/challan/downloaddisplay');
        }
    }
}
