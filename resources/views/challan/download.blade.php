
@extends('layouts.header')
@section('title','Download')
@section('styles')
<style>
    div.dataTables_wrapper {
        margin: 0 auto;
    }
</style>
@endsection
@section('datatables')
<link href="{{url('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">

@endsection
@section('content')


    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
                <div>
                    <h4 class="mg-b-0 tx-spacing--1">Downloads</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <table id="data_display" class="table display nowrap text-center">
                          <thead>
                            <tr>
                                <th>Name</th>
                                <th>Challan Created of</th>
                                <th>Link</th>
                                <th>Created At</th>
                            </tr>
                          </thead>
                            <tbody>

                            </tbody>
                        </table>
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection

@section('scripts')
<script src="{{url('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>


<script>
    $(document).ready(function () {
        $('#data_display').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[ 0, "desc" ]],
            "ajax":{
                     "url": "{{ url('admin/challan/challandownload') }}",
                     "dataType": "json",
                     "type": "POST",
                   },
            "columns": [
                { "data": "name" },
                { "data": "created" },
                { "data": "path" },
                { "data": "created_at" }
            ]


        });
    });
    function deleterow(id)
    {
        swal({
        title: 'Are you sure You want to delete?',
        icon: "warning",
        ClassName:"swal-title",
        showCancelButton: false,
        showConfirmButton: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var url = '{{url("/")}}';
                    $.ajax({
                    type: 'GET',
                    url:  url+'/admin/challan/delete/'+id,
                    success: function(data) {
                        if(data.code==200)
                        {
                            swalsuccesscurrent(data.message);
                        }
                        else
                        {
                            swalerror(data.message);
                        }
                    },
                    error: function(xhr) { // if error occured
                        swalerror("SomeThing Went Wrong");
                    },
                    });
            } else {
               return false;

            }
        });

    }
</script>

@endsection
