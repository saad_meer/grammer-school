<?php
use App\Models\Admin\Pages;
use App\Models\Admin\Author;
use Carbon\Carbon;
function db_format_date($date)
{
    if(!empty($date))
    {
        $formated_date=date('Y-m-d',strtotime($date));
        return $formated_date;
    }
}
function format_date($date)
{
    if(!empty($date))
    {
        $formated_date=date('d-m-Y',strtotime($date));
        return $formated_date;
    }
}

function format_date_time($date)
{
    if(!empty($date))
    {
        $formated_date=date('d-m-Y h:i a',strtotime($date));
        return $formated_date;
    }
}
function get_pages()
{
    $page=Pages::where('page_status','Active')->get();
    return $page;
}
function createSlug($name=''){
    $string = $name;
    $split = str_split($string, 3);
    $result =implode('-', $split);

    $slug = preg_replace('~[^\pL\d]+~u', '-', $name);

    // transliterate
    if (function_exists('iconv')) {
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
    }

    // remove unwanted characters
    $slug = preg_replace('~[^-\w]+~', '', $slug);

    // trim
    $slug = trim($slug, '-');

    // remove duplicate -
    $slug = preg_replace('~-+~', '-', $slug);

    // lowercase
    $slug = strtolower($slug);
    return $slug;
}
 function birthday()
{
   $birthday=Author::whereMonth('author_dob', '=', Carbon::now()->format('m'))->whereDay('author_dob', '=', Carbon::now()->format('d'))->get();
    return $birthday;
}

