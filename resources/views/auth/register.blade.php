<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DashForge">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{url('assets/img/favicon.png')}}">

    <title>Quoto | SignUp</title>

    <!-- vendor css -->
    <link href="{{url('lib/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{url('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="{{url('assets/css/dashforge.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/dashforge.auth.css')}}">
    <style>
        .swal-title {
        font-size:16px !important;
        }
    </style>
  </head>
  <body>

    <div class="content content-fixed content-auth">
      <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p">
          <div class="sign-wrapper mg-lg-r-50 mg-xl-r-60">
            <div class="pd-t-20 wd-100p">
              <h4 class="tx-color-01 mg-b-5">Create New Account</h4>
              <p class="tx-color-03 tx-16 mg-b-40">It's free to signup and only takes a minute.</p>
              <form method="POST"  id="signup" action="{{ url('register') }}">
                        @csrf
              <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control @error('name') parsley-error @enderror" value="{{ old('name') }}" name="first_name" placeholder="Enter your Name">
                @error('name')
                    <strong class="text-danger">{{ $message }}</strong>
                @enderror
              </div>
              <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control @error('name') parsley-error @enderror" value="{{ old('name') }}" name="last_name" placeholder="Enter your Name">
                @error('name')
                    <strong class="text-danger">{{ $message }}</strong>
                @enderror
              </div>
              <div class="form-group">
                <label>Email address</label>
                <input type="email" class="form-control  @error('email') parsley-error @enderror" name="email" value="{{ old('email') }}" placeholder="Enter your email address">
                @error('email')
                    <strong class="text-danger">{{ $message }}</strong>
                @enderror
            </div>
              <div class="form-group">
                <div class="d-flex justify-content-between mg-b-5">
                  <label class="mg-b-0-f">Password</label>
                </div>
                <input type="password" class="form-control @error('password')  parsley-error @enderror" name="password"" placeholder="Enter your password">
                @error('password')
                    <strong class="text-danger">{{ $message }}</strong>
                @enderror
              </div>
              <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="Enter your Confirm password">
              </div>

              <button class="btn btn-brand-02 btn-block">Create Account</button>
           </div>
           <div class="tx-13 mg-t-20 tx-center">Already have an account? <a href="{{ url('/') }}">Login</a></div>
          </div><!-- sign-wrapper -->
          <div class="media-body pd-y-30 pd-lg-x-50 pd-xl-x-60 align-items-center d-none d-lg-flex pos-relative">
            <div class="mx-lg-wd-500 mx-xl-wd-550">
              <img src="{{asset('assets/img/img15.png')}}" class="img-fluid" alt="">
            </div>
          </div><!-- media-body -->
        </div><!-- media -->
      </div><!-- container -->
    </div><!-- content -->


    <script src="{{url('lib/jquery/jquery.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{url('assets/sweetalert.js')}}"></script>

    <script>
        $('#signup').submit(function(event) {

  //prevent the form from submitting by default
  event.preventDefault();
  var frm = $('#signup');

  var formData = new FormData($(this)[0]);

  $.ajax({
      url: frm.attr('action'),
      type: 'POST',
      data: formData,
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function (data) {
          if(data.code==200)
          {
              var url = '{{url("/")}}';
              location.href = url+"/dashboard"
          }
          if(data.code==404)
          {
              swalerror(data.message);
          }
      },
      error: function (error) {
          swalerror("SomeThing Went Wrong");
      }
  });



  });
    </script>
  </body>
</html>
