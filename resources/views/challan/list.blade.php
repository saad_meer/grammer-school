
@extends('layouts.header')
@section('title','Students')
@section('styles')
<style>
    div.dataTables_wrapper {
        margin: 0 auto;
    }
</style>
@endsection
@section('datatables')
<link href="{{url('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">

@endsection
@section('content')


    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Challan</h4>
            </div>
            @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
            @endif
            <div class="d-none d-md-block">
                <a href="{{url('admin/challan/add')}}" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5 active"><i data-feather="plus" class="wd-10 mg-r-5"></i>Genrate Challan</a>
                <a href="{{url('admin/challan/update')}}" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5 active"><i data-feather="plus" class="wd-10 mg-r-5"></i>Update Challan</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                        <table id="data_display" class="table display nowrap text-center">
                          <thead>
                            <tr>
                                <th>Challan</th>
                                <th>Student</th>
                                <th>class</th>
                                <th>Month</th>
                                <th>Transport</th>
                                <th>Discount</th>
                                <th>Late fee</th>
                                <th>Amount Paid</th>
                                <th>Action</th>
                            </tr>
                          </thead>
                            <tbody>

                            </tbody>
                        </table>
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection

@section('scripts')
<script src="{{url('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>


<script>
    $(document).ready(function () {
        $('#data_display').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength": 100,
            "ajax":{
                     "url": "{{ url('admin/challan/display') }}",
                     "dataType": "json",
                     "type": "POST",
                   },
            "columns": [
                { "data": "challan_number" },
                { "data": "student_name" },
                { "data": "class" },
                { "data": "challan_month"},
                { "data": "transport_fee" },
                { "data": "discount_amount" },
                { "data": "late_fee_amount" },
                { "data": "amount_paid" },
                { "data": "options" }
            ]


        });
    });
    function deleterow(id)
    {
        swal({
        title: 'Are you sure You want to delete?',
        icon: "warning",
        ClassName:"swal-title",
        showCancelButton: false,
        showConfirmButton: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var url = '{{url("/")}}';
                    $.ajax({
                    type: 'GET',
                    url:  url+'/admin/challan/delete/'+id,
                    success: function(data) {
                        if(data.code==200)
                        {
                            swalsuccesscurrent(data.message);
                        }
                        else
                        {
                            swalerror(data.message);
                        }
                    },
                    error: function(xhr) { // if error occured
                        swalerror("SomeThing Went Wrong");
                    },
                    });
            } else {
               return false;

            }
        });

    }
</script>

@endsection
