<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use View;
use PDF;
use App\Models\Admin\Challan;
use App\Models\Admin\Download;
use App\Models\Admin\Student;
class MakePdfstoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $get_challan=Challan::where('class',$this->input['std_class'])
                    ->whereMonth('challan_month', date('m',strtotime($this->input['challan_month'])))
                    ->whereYear('challan_month',date('Y',strtotime($this->input['challan_month'])));

                if($this->input['student']!='All')
                {
                    $get_challan= $get_challan->where('student_id',$this->input['student']);
                }
            $get_challan=$get_challan->get();
            $html='';
            $challancount=count($get_challan);
            if(count($get_challan)>0)
            {

                foreach ($get_challan as $stdkey=>$challan)
                {
                    if($stdkey==0)
                    {
                        $view = view('challan.pdf.challan', array('header_not' => 0, 'footer_not' => 1, 'challan' => $challan));
                    }
                    elseif($stdkey == ($challancount-1))
                    {
                        $view = view('challan.pdf.challan', array('header_not' => 1, 'footer_not' => 1,  'challan' => $challan));
                    }
                    else
                    {
                        $view = view('challan.pdf.challan', array('header_not' => 1, 'footer_not' => 1, 'challan' => $challan));
                    }
                    $html .= $view->render();
                }
            }

        $pdf = PDF::loadHTML($html);
        $sheet = $pdf->setPaper('a4', 'landscape');
        $challan_name=$this->input['std_class'].'-'.date('F Y',strtotime($this->input['challan_month'])).'-'.time();
        $challan_path='pdf/'.$this->input['std_class'].'-'.date('F Y',strtotime($this->input['challan_month'])).'-'.time().'.pdf';
        $path = public_path('pdf').'/'.$this->input['std_class'].'-'.date('F Y',strtotime($this->input['challan_month'])).'-'.time().'.pdf';
        $pdf->save($path);


        $user='';
        if($this->input['student']!='All')
        {
            $user=Student::where('id',$this->input['student'])->first('student_name');
            $user=$user->student_name;
        }
        else
        {
            $user='All';
        }

        $download=new Download();
        $download->name=$challan_name;
        $download->path=$challan_path;
        $download->challan_created_of=$user;
        $download->save();

    }
}
