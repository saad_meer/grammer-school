<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Admin\Challan;
use App\Models\Admin\FeeStructure;
use App\Jobs\MakePdfstoreJob;
use App\Models\Admin\Download;
use App\Models\Admin\Student;
use View;
use PDF;
use Illuminate\Support\Testing\Fakes\BusFake;
use Illuminate\Contracts\Queue\ShouldBeUnique;


class MakeChallanStoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $get_student;
    public $input;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($get_student,$input)
    {
        $this->get_student = $get_student;
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $request=$this->input;
        $get_student=$this->get_student;
        $get_fee=FeeStructure::where('student_class',$request['std_class'])->first('class_fee');
        $html='';
        $challancount=count($get_student);
        foreach ($get_student as $stdkey=>$data)
        {
            $get_challan=Challan::where('student_id',$data->id)->whereMonth('challan_month', date('m',strtotime($request['challan_month'])))
            ->whereYear('challan_month',date('Y',strtotime($request['challan_month'])))->first();
            if($get_challan)
            {
                continue;
            }
            $total_fee=$get_fee->class_fee+$data->transport_fee+$request['miscellaneous'];
            $discounted_fee=($total_fee*$data->fee_discount)/100;
            $total_fee=$total_fee- $discounted_fee;

            $challan=new Challan();
            $challan->challan_number=$data->class.'-'.random_int(100000, 999999);
            $challan->registration_number=$data->registration_number;
            $challan->student_name=$data->student_name;
            $challan->father_name=$data->father_name;
            $challan->father_cnic=$data->father_cnic;
            $challan->class=$data->class;
            $challan->original_amount=$get_fee->class_fee;
            $challan->transport_fee=$data->transport_fee;
            $challan->fee_discount=$data->fee_discount;
            $challan->student_id=$data->id;
            $challan->challan_month=db_format_date($request['challan_month']);
            $challan->last_date=db_format_date($request['last_date']);
            $challan->discount_amount= $discounted_fee;
            $challan->amount_paid=$total_fee;
            $challan->miscellaneous=$request['miscellaneous'];
            $challan->challan_status='Unpaid';
            $challan->save();

        }
        $challan_name=$request['std_class'].'-'.date('F Y',strtotime($request['challan_month'])).'-'.time();

        $user='';
        if($request['student']!='All')
        {
            $user=$request['student'];
        }
        else
        {
            $user='All';
        }

        $download=new Download();
        $download->name=$challan_name;
        $download->challan_created_of=$user;
        $download->student_class=$request['std_class'];
        $download->downloads_challan_month=db_format_date($request['challan_month']);
        $download->save();

    }
}
