
@extends('layouts.header')
@section('title','Upgrade')
@section('styles')
<link href="{{url('lib/select2/css/select2.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Promote Students</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/challan/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Add Challan" class="df-example demo-forms">
                        <form id="addform" action="{{url('admin/students/promote')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Please Select Class</label>
                                        <select class="custom-select student_class" name="student_class">
                                            <option value="">Please Select Class</option>
                                            @foreach ($student_class as $student)
                                                <option value="{{$student->student_class}}">{{$student->student_class}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Please Select Student</label>
                                        <select class="custom-select select2"  multiple="multiple"  name="student[]" id="student">
                                            <option value="">Please Select Student</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Please Select Class to Promote</label>
                                        <select class="custom-select" name="student_upgrade_class">
                                            <option value="">Please Select Class</option>
                                            @foreach ($student_class as $student)
                                                <option value="{{$student->student_class}}">{{$student->student_class}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/select2/js/select2.min.js')}}"></script>

<script type="text/javascript">
$('.select2').select2({
  placeholder: 'Choose one',
  searchInputPlaceholder: 'Search options'
});
function genratechallan()
{

    var url = '{{url("/")}}';
    var std_class=$('.student_class option:selected').val();
    if(std_class==NaN || std_class=='')
    {
        swalerror('Please Select Student Class');
        return false;
    }
    var student=$('#student option:selected').val();

    if(student==NaN || student=='')
    {
        swalerror('Please Select Student');
        return false;
    }
    var challan_month=$('#challan_month').val();
    if(challan_month==NaN || challan_month=='')
    {
        swalerror('Please Select  Month');
        return false;
    }
    var last_date=$('#last_date').val();
    if(last_date==NaN || last_date=='')
    {
        swalerror('Please Select  Last Date');
        return false;
    }


    $.ajax({
    url: url+'/admin/challan/genratechallan',
    type: 'POST',
    data: {std_class:std_class,student:student,challan_month:challan_month,last_date:last_date},
    success: function (data) {
        if(data.code==200)
        {
            window.open(data.path, '_blank').focus();
            swalsuccess(data.message);

        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});

}

$(".student_class").change(function()
{
    $('#genrate_challan').addClass('update_challan');

    var html='';
    var url = '{{url("/")}}';
    var student_class = $('.student_class').val();
    $.ajax({
        url: url+'/admin/get_student_class',
        type: 'POST',
        data: {
            student_class: student_class
        },
        success: function (data) {
            if(data.code==200)
            {
                html+='<option value="All">All Students</option>';
                $.each( data.result, function( key, value ) {
                    html+='<option value="'+value.id+'">'+value.student_name+'</option>';
                });
                $('#student').html(html);
            }
            if(data.code==404)
            {
                html='<option value="">Please Select Student</option>';
                $('#student').html(html);
            }
        },
        error: function (error) {
        // swalerror("SomeThing Went Wrong");
        }
    });
});

$('#addform').submit(function(event) {
    var url = '{{url("/")}}';


//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addform')[0].reset();
            swalsuccess(data.message,url+'/admin/students/display');

        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



