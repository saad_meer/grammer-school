<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\FontStyle;
class FontsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \DB::table('fonts_style')->delete();

        \DB::table('fonts_style')->insert(array (
            0 =>
            array (
                'id' => 1,
                'font_style' => 'IBM Plex Sans',
            ),
            1 =>
            array (
                'id' => 2,
                'font_style' => 'Inter UI',
            ),
            2 =>
            array (
                'id' => 3,
                'font_style' => 'cursive',
            ),
            3 =>
            array (
                'id' => 4,
                'font_style' => 'fangsong',
            ),
            4 =>
            array (
                'id' => 5,
                'font_style' => 'math',
            ),
            5 =>
            array (
                'id' => 6,
                'font_style' => 'monospace',
            ),
            6 =>
            array (
                'id' => 7,
                'font_style' => 'sans-serif',
            ),
            7 =>
            array (
                'id' => 8,
                'font_style' => 'serif',
            ),
            8 =>
            array (
                'id' => 9,
                'font_style' => 'ui-monospace',
            ),
            9 =>
            array (
                'id' => 10,
                'font_style' => 'ui-sans-serif',
            ),
            10 =>
            array (
                'id' => 11,
                'font_style' => 'Poppins',
            ),
            11 =>
            array (
                'id' => 12,
                'font_style' => 'Roboto',
            ),
        ));
    }
}
