@php
use App\Models\Admin\Challan;
@endphp
@if(isset($student) && count($student)>0)

@foreach ($student as $data)
@php
$get_challan=Challan::where('student_id',$data->id)
->whereMonth('challan_month', date('m',strtotime($month)))
->whereYear('challan_month',date('Y',strtotime($month)))->first();
if($get_challan)
{
    continue;
}
@endphp
<tr>
    <th>{{$data->student_name}}</th>
    <th>{{$data->father_name}}</th>
    <th>{{$data->class}}</th>
    <th>{{date('M',strtotime($month))}}</th>
    <th>{{$get_fee->class_fee}}</th>
    <th>{{$miscellaneous}}</th>
    <th>{{isset($data->transport_fee)?$data->transport_fee:''}}</th>
    <th>@if(isset($data->fee_discount)){{$data->fee_discount}}% @endif</th>
    @php
    $total_fee=$get_fee->class_fee+$data->transport_fee+$miscellaneous;
    $discounted_fee=($total_fee*$data->fee_discount)/100;
    $total_fee=$total_fee- $discounted_fee;
    @endphp
    <th>{{isset($discounted_fee)?$discounted_fee:''}}</th>
    <th>{{ $total_fee}}</th>
</tr>
@endforeach
@else
<tr>
    <td scope="row" class="text-center" colspan="9">No record</td>
</tr>
@endif
