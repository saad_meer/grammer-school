<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Session;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login()
    {
         return view('auth.login');
    }
    public function loginProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ],
        [
            'email.required' => 'Email is Required',
            'password.required' => 'Password is Required',
            'g-recaptcha-response.required' => 'Captcha is Required',
         ]);
        if ($validator->fails()) {
            return redirect()->back()->with('success', $validator->errors()->first());
        }

         $auth = auth();
         $dataAttempt = array(
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );
         if (Auth::attempt($dataAttempt)) {
            if(auth()->user()->role == 'admin')
            {
                return redirect('/admin/dashboard');
            }
            else
            {
                return redirect('/admin/students/display');
            }
         } else {
            return redirect('/admin/login')->with('success','Credentials not matched');
         }

    }
    public function logout()
    {
        auth()->logout();
        return redirect('/admin/login');
    }
    public function signup()
    {
        return view('auth.register');
    }
    public function signupProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:users,email',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ],
        [
            'first_name.required' => 'First Name is Required',
            'last_name.required' => 'Last Name is Required',
            'email.required' => 'Email is Required',
            'password.required' => 'Password is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
         $user = User::insert([
             'first_name'=>$request->first_name,
             'last_name'=>$request->last_name,
             'email'=>$request->email,
             'password'=> Hash::make($request->password),
         ]);


          return response()->json(['code'=>200,'message'=>'']);

    }
}
