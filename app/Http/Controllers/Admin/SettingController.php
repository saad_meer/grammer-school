<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\FeeStructure;


class SettingController extends Controller
{
    public function setting()
    {
        $student_class=FeeStructure::all();
        return view('setting.add',compact('student_class'));
    }
    public function addsetting(Request $request)
    {

        $input=$request['student_fee'];
        if(isset($input) && !empty($input))
        {
            foreach($input as $key=>$value)
            {
                FeeStructure::where('student_class',$key)->update(['class_fee'=>$value]);
            }
            return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);

        }
    }
}
